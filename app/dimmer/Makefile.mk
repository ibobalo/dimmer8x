BOARD=dimmer8x
USE_PARAMS=YES
USE_TRIAC=YES
USE_RAMP=YES
USE_MODBUS=YES
USE_PCA9532=YES
#USE_RFM69=YES
USE_KEYSCANNER=YES
#USE_BOOTLOADER=YES
USE_USB_OTG=YES
USE_HW_IF=YES
USE_PROCESS_SCHEDULLER=YES

DEFINES += \
	LED_NET_LIVE=0 \
	LED_NET_RX=0 \
	LED_NET_TX=0 \
	LED_NET_PING=0 \
	LED_ZERO=7 \
	KEYS_COUNT=16 \
	TRIAC_CHANNEL_COUNT=8 \
	DIMMER_CHANNEL_COUNT=8 \
	USE_APP_PARAMS

SRC += $(addprefix $(APP_PATH)/, \
	app_init.cpp \
	proc_dimmer_ctrl.cpp \
)
