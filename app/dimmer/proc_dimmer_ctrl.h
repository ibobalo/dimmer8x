#ifndef PROCESS_DIMMER_CTRL_H_INCLUDED
#define PROCESS_DIMMER_CTRL_H_INCLUDED

#include "tick_process.h"

//extern
class ILed;
template<class T> class IKey;
template<class T> class IValueRequester;
template<class T, class Tout> class ITransformer;
class IModbusSlave;

class IDimmerControler
{
public:
	virtual void Init() = 0;
	virtual void AssignChannelLed(unsigned nChannel, ILed* pLed) = 0;
	virtual void AssignChannelKey(unsigned nChannel, IKey<ticktime_t>* pKey) = 0;
	virtual void AssignChannelOut(unsigned nChannel, IValueRequester<float>* pOut, ITransformer<float,float>* pTransformer) = 0;
	virtual void Control(bool bEnable) = 0;
};

IDimmerControler* GetDimmerControlerInterface();

#ifdef USE_MODBUS

IModbusSlave* AcquireDimmerModbusSlave(unsigned uiSlaveAddress);

#endif // USE_MODBUS

#endif // PROCESS_DIMMER_CTRL_H_INCLUDED
