#include "stdafx.h"
#include "hw/hw.h"
#include "util/macros.h"
#include "hw/hw_macros.h"
#include "tick_process.h"
#include "params.h"
#ifdef USE_RAMP
#  include "proc_ramp.h"
#  include "transformer.h"
#endif
#ifdef USE_TRIAC
#  include "zero_sync_pwm.h"
#endif
#ifdef USE_KEYSCANNER
#  include "proc_keyscanner.h"
#endif
#ifdef USE_MODBUS
#  include "proc_modbus.h"
#  include "modbus_slave.h"
#endif
#ifdef USE_USB
#  include "piped_uart.h"
#endif
#ifdef USE_PARAMS
#  include "modules/params/params.h"
#endif
#include "proc_dimmer_ctrl.h"
#include <stdio.h>
#include <stdint.h>

// globals

extern "C" {

void app_init(void)
{
	static const pair<float> aDimmerTransformation[] = {
			{0.0, 0.0},
			{0.5, 0.3},
			{1.0, 1.0},
	};

	ITransformer<float>* pDimmerTransformer = AcquireLinearTransformer(aDimmerTransformation, ARRAY_SIZE(aDimmerTransformation));
	ITriacZeroSyncPWM<float>* pTriacZeroSyncPWM = GetTriacZeroSyncPWMInterface();

	GetDimmerControlerInterface()->Init();

	for (unsigned nChannel = 0; nChannel < TRIAC_CHANNEL_COUNT; ++nChannel) {
		ILed* pLed = GetLedsInterface()->AcquireLedInterface(nChannel);
		if (pLed) {
			GetDimmerControlerInterface()->AssignChannelLed(nChannel, pLed);
		}
#ifdef USE_KEYSCANNER
		for (unsigned nKey = 0; nKey < GetKeyboardInterface()->GetKeysCount(); ++nKey) {
			uint8_t uiChannelAttachedToKey = GET_PARAM_VALUE(DIMMER_KEYS_ATTACHED_CHANNEL, nKey);
			if (uiChannelAttachedToKey == nChannel) {
				IKey<ticktime_t>* pKey = GetKeyboardInterface()->GetKeyInterface(nKey);
				GetDimmerControlerInterface()->AssignChannelKey(nChannel, pKey);
			}
		}
#endif // USE_KEYSCANNER
		for (unsigned nOut = 0; nOut < TRIAC_CHANNEL_COUNT; ++nOut) {
			uint8_t uiChannelAttachedToTriac = GET_PARAM_VALUE(DIMMER_TRIAC_ATTACHED_CHANNEL, nOut);
			if (uiChannelAttachedToTriac == nChannel) {
				ITriacChannel<float>* pTriac = pTriacZeroSyncPWM->GetTriacChannelInterface(nOut);
				if (pTriac) {
					GetDimmerControlerInterface()->AssignChannelOut(nChannel, pTriac, pDimmerTransformer);
				}
			}
		}
	}

#ifdef USE_USB_MODBUS
	ISerial* pSerial = CHW_MCU::AcquireUSBSerialInterface(0);
#else
	ISerial* pSerial = CHW_Brd::AcquireSerialInterface(0);
#endif // USE_USB
#ifdef USE_MODBUS
	uint8_t uiMBAddress = GET_PARAM_VALUE(DIMMER_MODBUS_SLAVE_ADDRESS);
	if (uiMBAddress == 0) {
		CHW_MCU::GetUniqueId().digest(uiMBAddress);
	}
	GetModbusSlaveProcessor()->Init(pSerial, AcquireDimmerModbusSlave(uiMBAddress));


#endif // USE_MODBUS
}

} // extern "C"
