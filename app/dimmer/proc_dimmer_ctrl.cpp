#include "stdafx.h"
#include "proc_dimmer_ctrl.h"
#include "util/macros.h"
#include "util/utils.h"
#include "interfaces/led.h"
#include "interfaces/key.h"
#include "interfaces/gpio.h"
#include "hw/hw.h"
#include "hw/debug.h"
#include "hw/hw_macros.h"
#include "tick_process.h"
#include "zero_sync_pwm.h"
#include "proc_keyscanner.h"
#include "proc_ramp.h"
#include "params.h"
#include <math.h> // fabs

class CDimmerChannel :
	public IKeyHandler<ticktime_t>
{
public:
	typedef unsigned CTimeType;
public:
	void Init(unsigned nChannel);
	void AssignLed(ILed* pLed);
	void AssignKey(IKey<ticktime_t>* pKey);
	void AssignOut(IValueRequester<float>* pOut, ITransformer<float>* pTransformer = NULL);

public: // IKeyHandler<ticktime_t>
	virtual void OnButtonPress(ticktime_t ttTime);
	virtual void OnButtonRelease(ticktime_t ttTime);
	virtual void OnButtonHold(ticktime_t ttTime);

public:
	bool GetKeyState() const {return m_bKeyState;};
	float GetDimmerDefaultValue() const {return m_dDimmerDefaultValue;}
	void  SetDimmerDefaultValue(float dDefaultValue) {m_dDimmerDefaultValue = dDefaultValue;}
	float GetDimmerDefaultIncRate() const {return m_dDimmerIncRate;}
	void  SetDimmerDefaultIncRate(float dDefaultIncRate) {m_dDimmerIncRate = dDefaultIncRate;}
	float GetDimmerDefaultDecRate() const {return m_dDimmerDecRate;}
	void  SetDimmerDefaultDecRate(float dDefaultDecRate) {m_dDimmerDecRate = dDefaultDecRate;}
	float GetDimmerFinalValue() const {return m_pRamp->GetFinalValue();}
	float GetDimmerCurrentValue() const {return m_pRamp->GetCurrentValue();}
	void  SetDimmerFinalValue(ticktime_t ttTime, float dValue);
private:
	unsigned      m_nChannel;
	float         m_dDimmerDefaultValue;
	float         m_dDimmerMinValue;
	float         m_dDimmerMaxValue;
	float         m_dDimmerIncRate;
	float         m_dDimmerDecRate;
	bool          m_bDimmerLastDirectionIsUp;
	bool          m_bCyclingUpDown;
	CTimeType     m_ttCyclingStopTime;
	unsigned      m_nKeyCounter;
	bool          m_bKeyState;
	bool          m_bDInState;
	ILed*         m_pLed;
	IRamp<float>* m_pRamp;
};

class CDimmerControlProcess :
	public IDimmerControler,
//	public IKeyboardHandler<ticktime_t>,
	public CTickProcess
{
public:
	typedef CTickProcess CParentProcess;
//	~CDimmerControlProcess();

public: // IDimmerControler
	virtual void Init();
	virtual void AssignChannelLed(unsigned nChannel, ILed* pLed);
	virtual void AssignChannelKey(unsigned nChannel, IKey<ticktime_t>* pKey);
	virtual void AssignChannelOut(unsigned nChannel, IValueRequester<float>* pOut, ITransformer<float>* pTransformer);
	virtual void Control(bool bEnable);

public: // CTickProcess
	virtual void SingleStep(CTimeType uiTime);

//public: // IKeyboardHandler<ticktime_t>
//	virtual void OnKeyEvent(unsigned nKey, EKeyEvent eEvent, ticktime_t ttTime);

public:
	bool GetKeyState(unsigned uiKeyIndex) const {ASSERTE(uiKeyIndex < ARRAY_SIZE(m_aDimmerControls)); return m_aDimmerControls[uiKeyIndex].GetKeyState();}
	float GetDimmerDefaultValue(unsigned uiChannel) const {return m_aDimmerControls[uiChannel].GetDimmerDefaultValue();}
	void  SetDimmerDefaultValue(unsigned uiChannel, float dDefaultValue) {return m_aDimmerControls[uiChannel].SetDimmerDefaultValue(dDefaultValue);}
	float GetDimmerDefaultIncRate(unsigned uiChannel) const {return m_aDimmerControls[uiChannel].GetDimmerDefaultIncRate();}
	void  SetDimmerDefaultIncRate(unsigned uiChannel, float dDefaultIncRate) {return m_aDimmerControls[uiChannel].SetDimmerDefaultIncRate(dDefaultIncRate);}
	float GetDimmerDefaultDecRate(unsigned uiChannel) const {return m_aDimmerControls[uiChannel].GetDimmerDefaultDecRate();}
	void  SetDimmerDefaultDecRate(unsigned uiChannel, float dDefaultDecRate) {return m_aDimmerControls[uiChannel].SetDimmerDefaultDecRate(dDefaultDecRate);}
	float GetDimmerFinalValue(unsigned uiChannel) const {return m_aDimmerControls[uiChannel].GetDimmerFinalValue();}
	void  SetDimmerFinalValue(unsigned uiChannel, float dValue) {return m_aDimmerControls[uiChannel].SetDimmerFinalValue(GetTimeNow(), dValue);}
	float GetDimmerCurrentValue(unsigned uiChannel) const {return m_aDimmerControls[uiChannel].GetDimmerCurrentValue();}

private:
	unsigned           m_uiCurrentChannel;
	CDimmerChannel  m_aDimmerControls[TRIAC_CHANNEL_COUNT];
};


void CDimmerChannel::Init(unsigned nChannel)
{
	ASSERTE(nChannel < TRIAC_CHANNEL_COUNT);
	m_nChannel = nChannel;
	m_dDimmerDefaultValue = GET_PARAM_VALUE(DIMMER_CHANNELS_DEFAULT_VALUE,    nChannel);
	m_dDimmerMinValue     = GET_PARAM_VALUE(DIMMER_CHANNELS_MIN_VALUE,        nChannel);
	m_dDimmerMaxValue     = GET_PARAM_VALUE(DIMMER_CHANNELS_MAX_VALUE,        nChannel);
	m_dDimmerIncRate      = GET_PARAM_VALUE(DIMMER_CHANNELS_DEFAULT_INC_RATE, nChannel);
	m_dDimmerDecRate      = GET_PARAM_VALUE(DIMMER_CHANNELS_DEFAULT_DEC_RATE, nChannel);
	m_bDimmerLastDirectionIsUp = false;
	m_bCyclingUpDown = false;
	m_nKeyCounter = 0;
	m_pRamp = AcquireRampProcess(0.005, 20);
}

void CDimmerChannel::AssignLed(ILed* pLed)
{
	m_pLed = pLed;
}

void CDimmerChannel::AssignKey(IKey<ticktime_t>* pKey)
{
	if (pKey) {
		pKey->AssignKeyHandler(this);
	}
}

void CDimmerChannel::AssignOut(IValueRequester<float>* pOut, ITransformer<float>* pDimmerTransformer/*= NULL*/)
{
	pOut->SetValueSourceInterface(m_pRamp);
}

void  CDimmerChannel::SetDimmerFinalValue(ticktime_t ttTime, float dValue)
{
	float dv = dValue - m_pRamp->GetCurrentValue();
	float rate;
	if (dv > 0) {
		rate = GetDimmerDefaultIncRate();
	} else if (dv < 0) {
		dv = -dv;
		rate = GetDimmerDefaultDecRate();
	} else {
		m_pRamp->SetValueInstant(dValue);
		return;
	}
	ticktime_t ttPeriod = CTickTimeSource::GetDeltaFromSeconds(dv / rate);
	DEBUG_INFO("Dimming period:", ttPeriod);
	m_pRamp->SetValueWithEndTime(dValue, ttTime + ttPeriod);
}

void CDimmerChannel::OnButtonPress(ticktime_t ttTime)
{
	m_bKeyState = true;
	++m_nKeyCounter;

	// switch on/off
	if (m_bDimmerLastDirectionIsUp) {
		DEBUG_INFO("Dimming OFF by Key Press ch:", m_nChannel);
		SetDimmerFinalValue(ttTime, 0.0);
		m_bDimmerLastDirectionIsUp = false;
	} else {
		DEBUG_INFO("Dimming ON by Key Press %:", 100 * m_dDimmerDefaultValue);
		SetDimmerFinalValue(ttTime, m_dDimmerDefaultValue);
		m_bDimmerLastDirectionIsUp = true;
	}

	if (m_pLed) m_pLed->Set();
}

void CDimmerChannel::OnButtonRelease(ticktime_t ttTime)
{
	m_bKeyState = false;

	if (m_bCyclingUpDown) {
		m_pRamp->Stop();
		m_dDimmerDefaultValue = m_pRamp->GetCurrentValue();
		m_bCyclingUpDown = false;
	}
	if (m_pLed) m_pLed->Clear();
}

void CDimmerChannel::OnButtonHold(ticktime_t ttTime)
{
	if (!m_bCyclingUpDown) {
		// start cycling - switch speed
		m_bCyclingUpDown = true;
		double dValue = m_bDimmerLastDirectionIsUp ? 1.0 : 0.01;
		m_pRamp->SetValueWithPeriod(dValue, fabs(dValue - m_pRamp->GetCurrentValue()) / 10000.0);
		if (m_pLed) m_pLed->FastBlink();
	} else if (!m_pRamp->IsStopped()) {
		// continue cycling
		m_ttCyclingStopTime = ttTime;
	} else {
		// change directions - UP/DOWN
		if (m_bDimmerLastDirectionIsUp) {
			if ((int)(ttTime - m_ttCyclingStopTime) < 2000) {
				// pause on top
				if (m_pLed) m_pLed->Set();
			} else {
				// go down after pause
				DEBUG_INFO("Dimming Down by Key Hold");
				m_pRamp->SetValueWithEndTime(0.01, ttTime + 10000);
				m_bDimmerLastDirectionIsUp = false;
				if (m_pLed) m_pLed->FastBlink();
			}
		} else {
			if ((int)(ttTime - m_ttCyclingStopTime) < 1000) {
				// pause on top
				if (m_pLed) m_pLed->Clear();
			} else {
				// go up after pause
				DEBUG_INFO("Dimming Up by Key Hold");
				m_pRamp->SetValueWithEndTime(1.0, ttTime + 10000);
				m_bDimmerLastDirectionIsUp = true;
				if (m_pLed) m_pLed->FastBlink();
			}
		}
	}
}

void CDimmerControlProcess::Init()
{
	IMPLEMENTS_INTERFACE_METHOD(IDimmerControler::Init());

	CParentProcess::Init(&g_TickProcessScheduller);

	m_uiCurrentChannel = 0;
	for (unsigned n = 0; n < ARRAY_SIZE(m_aDimmerControls); ++n) {
		m_aDimmerControls[n].Init(n);
	}
	Control(true);
}

void CDimmerControlProcess::AssignChannelLed(unsigned nChannel, ILed* pLed)
{
	IMPLEMENTS_INTERFACE_METHOD(IDimmerControler::AssignChannelLed(nChannel, pLed));

	ASSERTE(nChannel < ARRAY_SIZE(m_aDimmerControls));

	m_aDimmerControls[nChannel].AssignLed(pLed);
}

void CDimmerControlProcess::AssignChannelKey(unsigned nChannel, IKey<ticktime_t>* pKey)
{
	IMPLEMENTS_INTERFACE_METHOD(IDimmerControler::AssignChannelKey(nChannel, pKey));

	ASSERTE(nChannel < ARRAY_SIZE(m_aDimmerControls));

	m_aDimmerControls[nChannel].AssignKey(pKey);
}

void CDimmerControlProcess::AssignChannelOut(unsigned nChannel, IValueRequester<float>* pOut, ITransformer<float>* pTransformer)
{
	IMPLEMENTS_INTERFACE_METHOD(IDimmerControler::AssignChannelOut(nChannel, pOut, pTransformer));

	ASSERTE(nChannel < ARRAY_SIZE(m_aDimmerControls));

	m_aDimmerControls[nChannel].AssignOut(pOut, pTransformer);
}

void CDimmerControlProcess::Control(bool bEnable)
{
	IMPLEMENTS_INTERFACE_METHOD(IDimmerControler::Control(bEnable));

	if (bEnable) {
		ContinueAsap();
	} else {
		Cancel();
	}
}

void CDimmerControlProcess::SingleStep(CTimeType ttTime)
{
	IMPLEMENTS_INTERFACE_METHOD(CTickProcess::SingleStep(ttTime));
	PROCESS_DEBUG_INFO(">>> Process: CDimmerControlProcess");

	m_uiCurrentChannel++;
	if (m_uiCurrentChannel >= ARRAY_SIZE(m_aDimmerControls)) {
		m_uiCurrentChannel = 0;
	}

	ContinueAt(ttTime + 10);

	PROCESS_DEBUG_INFO("<<< Process: CDimmerControlProcess");
}

///*virtual*/
//void CDimmerControlProcess::OnKeyEvent(unsigned nKey, EKeyEvent eEvent, ticktime_t ttTime)
//{
//	IMPLEMENTS_INTERFACE_METHOD(IKeyboardHandler<ticktime_t>::OnKeyEvent(nKey, eEvent, ttTime));
//
//}

//IKeyHandler<ticktime_t>* CDimmerControlProcess::GetKeyHandler(unsigned nKey)
//{
//	IMPLEMENTS_INTERFACE_METHOD(IKeyHandlersGroupProvider::GetKeyHandler(nKey));
//
//	if (nKey < ARRAY_SIZE(m_aDimmerControls)) {
//		return &m_aDimmerControls[nKey];
//	}
//	return NULL;
//}

CDimmerControlProcess g_DimmerControlProcess;

IDimmerControler* GetDimmerControlerInterface() {return &g_DimmerControlProcess;};

#ifdef USE_MODBUS
#include "modbus_slave.h"

/*
 * DI0   - DI7    - input keys
 * AIO0  - AIO7  - dimmer channel final values (0xFFFF = 1.0)
 * AIO8  - AIO15 - dimmer channel default values (0xFFFF = 1.0)
 * AIO16 - AIO23 - dimmer channel inc rate (1000 = 1/s)
 * AIO24 - AIO31 - dimmer channel dec rate (1000 = 1/s)
 * AIO32 - AIO39 - dimmer channel current values
 */
class CDimmerModbusSlave :
		public CModbusSlaveStub
{
public:
	void Init(uint16_t uiSlaveAddress, CDimmerControlProcess* pDimmerController) {
		m_pDimmerController = pDimmerController;
		CModbusSlaveStub::Init(uiSlaveAddress);
	};
public: // IModbusSlave
	virtual const char* GetSlaveId() const;
	virtual const uint8_t* GetSlaveIdExtra(unsigned* puiRetLength) const;
	virtual ModbusResultCode GetDiscreteInputs(unsigned uiInputAddr, bool* pbRetValue) const;
	virtual ModbusResultCode GetHoldingRegister(unsigned uiInputAddr, uint16_t* puiRetValue) const;
	virtual ModbusResultCode SetHoldingRegister(unsigned uiRegisterAddr, uint16_t uiValue);
private:
	CDimmerControlProcess* m_pDimmerController;
};

#define REGS(addr, r) \
		if (addr >= r && (nChannel = addr - r) < TRIAC_CHANNEL_COUNT)

/*virtual*/
const char* CDimmerModbusSlave::GetSlaveId() const
{
	OVERRIDE_METHOD(CModbusSlaveStub::GetSlaveId());
	return "Dimmer";
}

/*virtual*/
const uint8_t* CDimmerModbusSlave::GetSlaveIdExtra(unsigned* puiRetLength) const
{
	OVERRIDE_METHOD(CModbusSlaveStub::GetSlaveIdExtra(puiRetLength));
	ASSERTE(puiRetLength);
	const CHW_MCU::uniqid_t* pID = &(CHW_MCU::GetUniqueId());
	*puiRetLength = sizeof(*pID);
	return (const uint8_t*)pID;
}

/*virtual*/
ModbusResultCode CDimmerModbusSlave::GetDiscreteInputs(unsigned uiInputAddr, bool* pbRetValue) const
{
	OVERRIDE_METHOD(CModbusSlaveStub::GetDiscreteInputs(uiInputAddr, pbRetValue));
	unsigned nChannel;
	REGS(uiInputAddr, 0) { // Key
		*pbRetValue = m_pDimmerController->GetKeyState(nChannel);
		return MBRC_OK;
	}
	return MBRC_ILLEGAL_ADDR;
};

/*virtual*/
ModbusResultCode CDimmerModbusSlave::GetHoldingRegister(unsigned uiInputAddr, uint16_t* puiRetValue) const
{
	OVERRIDE_METHOD(CModbusSlaveStub::GetHoldingRegister(uiInputAddr, puiRetValue));
	unsigned nChannel;
	REGS(uiInputAddr, 0) { // Duty Cycle
		*puiRetValue = (uint16_t)(g_DimmerControlProcess.GetDimmerFinalValue(nChannel) * 0xFFFF);
		return MBRC_OK;
	}
	REGS(uiInputAddr, 16) { // Brightness
		*puiRetValue = (uint16_t)(g_DimmerControlProcess.GetDimmerDefaultValue(nChannel) * 0xFFFF);
		return MBRC_OK;
	}
	REGS(uiInputAddr, 32) { // Ramp Time
		*puiRetValue = (uint16_t)(g_DimmerControlProcess.GetDimmerDefaultIncRate(nChannel) * 1000.0);
		return MBRC_OK;
	}
	REGS(uiInputAddr, 48) { // Ramp to Brightness
		*puiRetValue = (uint16_t)(g_DimmerControlProcess.GetDimmerDefaultDecRate(nChannel) * 1000.0);
		return MBRC_OK;
	}
	REGS(uiInputAddr, 64) { //
		*puiRetValue = (uint16_t)(g_DimmerControlProcess.GetDimmerCurrentValue(nChannel) * 0xFFFF);
		return MBRC_OK;
	}
	return MBRC_ILLEGAL_ADDR;
}

/*virtual*/
ModbusResultCode CDimmerModbusSlave::SetHoldingRegister(unsigned uiRegisterAddr, uint16_t uiValue)
{
	OVERRIDE_METHOD(CModbusSlaveStub::SetHoldingRegister(uiRegisterAddr, uiValue));
	unsigned nChannel;
	REGS(uiRegisterAddr, 0) { // Duty Cycle
		float dValue = float(uiValue) / 0xFFFF;
		g_DimmerControlProcess.SetDimmerFinalValue(nChannel, dValue);
		return MBRC_OK;
	}
	REGS(uiRegisterAddr, 16) { // Brightness
		float dValue = float(uiValue) / 0xFFFF;
		g_DimmerControlProcess.SetDimmerDefaultValue(nChannel, dValue);
		return MBRC_OK;
	}
	REGS(uiRegisterAddr, 32) { // Ramp Time
		float dValue = float(uiValue) / 1000.0;
		g_DimmerControlProcess.SetDimmerDefaultIncRate(nChannel, dValue);
		return MBRC_OK;
	}
	REGS(uiRegisterAddr, 48) { // Ramp to Brightness
		float dValue = float(uiValue) / 1000.0;
		g_DimmerControlProcess.SetDimmerDefaultDecRate(nChannel, dValue);
		return MBRC_OK;
	}
	return MBRC_ILLEGAL_ADDR;
}

CDimmerModbusSlave g_DimmerModbusSlave;
IModbusSlave* AcquireDimmerModbusSlave(unsigned uiSlaveAddress) {g_DimmerModbusSlave.Init(uiSlaveAddress, &g_DimmerControlProcess); return &g_DimmerModbusSlave;};
#endif // USE_MODBUS
