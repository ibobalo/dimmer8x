#include "stdafx.h"
#include "proc_show.h"
#include "util/utils.h"
#include "util/macros.h"
#include "util/random.h"
#include "interfaces/led.h"
#include "interfaces/key.h"
#include "interfaces/display.h"
#include "hw/hw.h"
#include "tick_process.h"
#include "proc_ramp.h"
#include "color.h"
#include <stdio.h>

#define SHOW_CHANNEL_COUNT 7

class CNozzleProcessBase :
	public CTickProcess
{
public:
	typedef CTickProcess CParentProcess;
public:
	void Init(CRampPump* pRampPump, CRampLeds* pRampLeds);
	void Play(CTimeDelta uiInitialDelay);
	void Stop(float fDuration = 0.0);

	void SetPump(float fValue) { GetPumpRamp()->SetValueInstant(fValue);}
	void RampPump(float fValue, float fDuration) { GetPumpRamp()->SetValueWithPeriod(fValue, CParentProcess::CTimeSource::GetDeltaFromSeconds(fDuration));}
	void SetColor(CColorHSV tValue) { GetLedsRamp()->SetValueInstant(tValue);}
	void RampColor(CColorHSV tValue, float fDuration) { GetLedsRamp()->SetValueWithPeriod(tValue, CParentProcess::CTimeSource::GetDeltaFromSeconds(fDuration));}

protected: // CNozzleProcessBase
	virtual CTimeDelta Step(unsigned nStep) = 0;
public: // CTickProcess
	virtual void SingleStep(CTimeType ttTime);
protected:
	void SetStepsCount(unsigned nStepsCount);
	CRampPump*  GetPumpRamp() const {return m_pRampPump;}
	CRampLeds*  GetLedsRamp() const {return m_pRampLeds;}
private:
	CRampPump* m_pRampPump;
	CRampLeds* m_pRampLeds;
	unsigned   m_nStepsCount;
	unsigned   m_nStep;
};

void CNozzleProcessBase::Init(CRampPump* pRampPump, CRampLeds* pRampLeds)
{
	CParentProcess::Init(&g_TickProcessScheduller);

	m_pRampPump = pRampPump;
	m_pRampLeds = pRampLeds;
	m_nStepsCount = 0;
	m_nStep = 0;
}

void CNozzleProcessBase::Play(CTimeDelta uiInitialDelay)
{
	m_nStep = 0;
	ContinueDelay(uiInitialDelay);
}

void CNozzleProcessBase::Stop(float fDuration)
{
	m_nStep = 0;
	SetPump(0.0);
	if (fDuration == 0.0) {
		SetColor(make_color_hsv(0.0, 0.0, 0.0));
	} else {
		RampColor(make_color_hsv(0.0, 0.0, 0.0), fDuration);
	}
	Cancel();
}

/*virtual*/
void CNozzleProcessBase::SingleStep(CTimeType ttTime)
{
	IMPLEMENTS_INTERFACE_METHOD(CTickProcess::SingleStep(ttTime));
	CTimeDelta tdNextDelay = Step(m_nStep);
	++m_nStep;
	if (m_nStepsCount) { m_nStep %= m_nStepsCount; }
	if (tdNextDelay) ContinueAt(ttTime + tdNextDelay);
}

class CNozzleProcessTrapezoid :
		public CNozzleProcessBase
{
public:
	void SetTrapesoid(
			float fPumpPower,
			float fStartColor, float fTop1Color, float fTop2Color, float fEndColor,
			float fRaiseDuration, float fTopDuration, float fFallDuration, float fRestDuration
	);
protected: // CNozzleProcessBase
	virtual CTimeDelta Step(unsigned nStep);
private:
	float       m_fTopPumpPower;
	float       m_fStartColor;
	float       m_fTop1Color;
	float       m_fTop2Color;
	float       m_fEndColor;
	CTimeDelta  m_tpRaiseDuration;
	CTimeDelta  m_tpTopDuration;
	CTimeDelta  m_tpFallDuration;
	CTimeDelta  m_tpRestDuration;
	CTimeDelta  m_tpExtraLedsDuration;
};

void CNozzleProcessTrapezoid::SetTrapesoid(
		float fPumpPower,
		float fStartColor, float fTop1Color, float fTop2Color, float fEndColor,
		float fRaiseDuration, float fTopDuration, float fFallDuration, float fRestDuration
) {
	m_fTopPumpPower = fPumpPower;
	m_fStartColor = fStartColor;
	m_fTop1Color = fTop1Color;
	m_fTop2Color = fTop2Color;
	m_fEndColor = fEndColor;
	m_tpRaiseDuration = CParentProcess::CTimeSource::GetDeltaFromSeconds(fRaiseDuration);
	m_tpTopDuration = CParentProcess::CTimeSource::GetDeltaFromSeconds(fTopDuration);
	m_tpFallDuration = CParentProcess::CTimeSource::GetDeltaFromSeconds(fFallDuration);
	m_tpRestDuration = CParentProcess::CTimeSource::GetDeltaFromSeconds(fRestDuration);
	m_tpExtraLedsDuration = (fFallDuration < 1.0)
			? CParentProcess::CTimeSource::GetDeltaFromSeconds(1.0 - fFallDuration)
			: 0;
}

/*virtual*/
CNozzleProcessTrapezoid::CTimeDelta CNozzleProcessTrapezoid::Step(unsigned nStep)
{
	IMPLEMENTS_INTERFACE_METHOD(CNozzleProcessBase::Step(nStep));
	switch(nStep) {
	case 0:
		// raising
		GetPumpRamp()->SetValueWithPeriod(m_fTopPumpPower, m_tpRaiseDuration);
		GetLedsRamp()->SetValueInstant(make_color_hsv(m_fStartColor, 0.0, 1.0));
		GetLedsRamp()->SetValueWithPeriod(make_color_hsv(m_fTop1Color, 0.0, 1.0), m_tpRaiseDuration);
		return m_tpRaiseDuration;
		break;
	case 1:
		// top
		GetLedsRamp()->SetValueWithPeriod(make_color_hsv(m_fTop2Color, 0.0, 1.0), m_tpRaiseDuration);
		return m_tpTopDuration;
		break;
	case 2:
		// fall
		GetPumpRamp()->SetValueWithPeriod(0.0, m_tpRaiseDuration);
		GetLedsRamp()->SetValueWithPeriod(make_color_hsv(m_fEndColor, 0.0, 1.0), m_tpRaiseDuration);
		return m_tpFallDuration;
		break;
	}
	// rest
	if (m_tpExtraLedsDuration) {
		GetLedsRamp()->SetValueWithPeriod(make_color_hsv(m_fEndColor, 0.0, 0.0), m_tpExtraLedsDuration);
	} else {
		GetLedsRamp()->SetValueInstant(make_color_hsv(m_fStartColor, 0.0, 1.0));
	}
	return m_tpRestDuration;
}


class CShowProgram_Base :
		public IPlaylistEntry,
		public IPlayer,
		public IKeyHandler<ticktime_t>
{
public:
	void Init(
			IPlaylist* pParent,
			CRampPump* apPumpRamps[],
			CRampLeds* apLightRamps[],
			ILed*      pLed,
			IKey<ticktime_t>* pKey,
			ITextDisplayDeviceMonochrome* pDisplay
	);
public: // CShowProgram_Base
	virtual unsigned ProgramStep() = 0;

public: // IPlaylistEntry
	virtual void Enable();
	virtual void Disable();
	virtual bool IsEnabled() const;
public: // IPlayer
	virtual void Play();
	virtual void Stop();
	virtual bool IsPlaying() const;
	virtual void SetPlayerNotificationListener(IPlayerNotificationListener* pListener);
public: // IKeyHandler<ticktime_t>
	virtual void OnButtonPress(ticktime_t ttTime);
	virtual void OnButtonRelease(ticktime_t ttTime);
	virtual void OnButtonHold(ticktime_t ttTime);

protected:
	void DoRepeat() {
		++m_nRepeatCounter;
		m_nStepSub = 0;
		if(m_pDisplay) m_pDisplay->PutChar('.', true);
	}
private:
	void UpdateLed();
	void UpdateDisplay();

	IPlaylist*    m_pParent;
	ILed*         m_pLed;
	ITextDisplayDeviceMonochrome* m_pDisplay;
	bool          m_bEnabled;
	bool          m_bPlaying;
	bool          m_bKeyProcessed;
	IPlayerNotificationListener* m_pPlayerNotificationListener;
protected:
	unsigned      m_nStepSub;
	unsigned      m_nRepeatCounter;
	CNozzleProcessTrapezoid m_aNozzle[SHOW_CHANNEL_COUNT];
};

void CShowProgram_Base::Init(
		IPlaylist* pParent,
		CRampPump* apPumpRamps[],
		CRampLeds* apLightRamps[],
		ILed*      pLed,
		IKey<ticktime_t>* pKey,
		ITextDisplayDeviceMonochrome* pDisplay
) {
	m_pParent = pParent;
	m_pLed = pLed;
	m_pDisplay = pDisplay;
	if (pKey) pKey->AssignKeyHandler(this);
	m_bEnabled = true;
	m_bPlaying = false;
	m_bKeyProcessed = false;
	for (unsigned n = 0; n < ARRAY_SIZE(m_aNozzle); ++n) {
		m_aNozzle[n].Init(apPumpRamps[n], apLightRamps[n]);
	}
	UpdateLed();
	UpdateDisplay();
}

void CShowProgram_Base::SetPlayerNotificationListener(IPlayerNotificationListener* pListener)
{
	m_pPlayerNotificationListener = pListener;
}

void CShowProgram_Base::Enable()
{
	IMPLEMENTS_INTERFACE_METHOD(IPlaylistEntry::Enable());

	m_bEnabled = true;
	UpdateLed();
	UpdateDisplay();
}

void CShowProgram_Base::Disable()
{
	IMPLEMENTS_INTERFACE_METHOD(IPlaylistEntry::Disable());

	m_bEnabled = false;
	UpdateLed();
	UpdateDisplay();
}

bool CShowProgram_Base::IsEnabled() const
{
	IMPLEMENTS_INTERFACE_METHOD(IPlaylistEntry::IsEnabled());

	return m_bEnabled;
}

void CShowProgram_Base::Play()
{
	IMPLEMENTS_INTERFACE_METHOD(IPlayer::Play());

	if (!m_bPlaying) {
		if (m_pPlayerNotificationListener) m_pPlayerNotificationListener->OnPlayStarts();
		m_bPlaying = true;
		m_nStepSub = 0;
		m_nRepeatCounter = 0;
		UpdateLed();
		UpdateDisplay();
	}
}

bool CShowProgram_Base::IsPlaying() const
{
	IMPLEMENTS_INTERFACE_METHOD(IPlayer::IsPlaying());
	return m_bPlaying;
}

void CShowProgram_Base::Stop()
{
	IMPLEMENTS_INTERFACE_METHOD(IPlayer::Stop());

	if (m_bPlaying) {
		for (unsigned n = 0; n < SHOW_CHANNEL_COUNT; ++n) {
			m_aNozzle[n].Stop(1.0);
		}
		m_bPlaying = false;
		UpdateLed();
		UpdateDisplay();
		if (m_pPlayerNotificationListener) m_pPlayerNotificationListener->OnPlayStops();
	}
}

void CShowProgram_Base::UpdateLed()
{
	if (m_pLed) {
		if (m_bEnabled) {
			if (m_bPlaying) {
				m_pLed->Blink();
			} else {
				m_pLed->Set();
			}
		} else {
			m_pLed->Clear();
		}
	}
}

void CShowProgram_Base::UpdateDisplay()
{
	if (m_pDisplay) {
		if (m_bEnabled) {
			if (m_bPlaying) {
			} else {
			}
		} else {
		}
	}
}

void CShowProgram_Base::OnButtonPress(ticktime_t ttTime)
{
	IMPLEMENTS_INTERFACE_METHOD(IKeyHandler<ticktime_t>::OnButtonPress(ttTime));

	m_bKeyProcessed = false;
}

void CShowProgram_Base::OnButtonRelease(ticktime_t ttTime)
{
	IMPLEMENTS_INTERFACE_METHOD(IKeyHandler<ticktime_t>::OnButtonRelease(ttTime));

	if (!m_bKeyProcessed) {
		m_bKeyProcessed = true;
		m_pParent->PlayItem(this);
	}
}

void CShowProgram_Base::OnButtonHold(ticktime_t ttTime)
{
	IMPLEMENTS_INTERFACE_METHOD(IKeyHandler<ticktime_t>::OnButtonHold(ttTime));

	if (!m_bKeyProcessed) {
		m_bKeyProcessed = true;
		if (IsEnabled()) {
			Disable();
		} else {
			Enable();
		}
	}
}


class CShowProgram_AllTogeza:
	public CShowProgram_Base
{
public: // CShowProgram_Base
	virtual unsigned ProgramStep();
};
unsigned CShowProgram_AllTogeza::ProgramStep()
{
	IMPLEMENTS_INTERFACE_METHOD(CShowProgram_Base::ProgramStep());

	unsigned uiContinueAfterMs = 0;

	while (IsEnabled() && m_nRepeatCounter < 10) {
		if (m_nStepSub < 2) {
			for (unsigned n = 0; n < SHOW_CHANNEL_COUNT; ++n) {
				m_aNozzle[n].RampPump(m_nStepSub?0.0:1.0, 5.0);
				m_aNozzle[n].SetColor(make_color_hsv(0.0, 1.0, 1.0));
				m_aNozzle[n].RampColor(make_color_hsv(1.0, 1.0, 1.0), 5.0);
			}
			++m_nStepSub;
			uiContinueAfterMs = 5000;
			break;
		} else {
			DoRepeat();
		}
	}

	return uiContinueAfterMs;
}
CShowProgram_AllTogeza g_ShowProgram_AllTogeza;

class CShowProgram_FastShoots:
	public CShowProgram_Base
{
public: // CShowProgram_Base
	virtual unsigned ProgramStep();
};

static const float g_aColorHues[] = {0, 0.083, 0.25, 0.333, 0.5, 0.667, 0.75, 0.833};
unsigned CShowProgram_FastShoots::ProgramStep()
{
	IMPLEMENTS_INTERFACE_METHOD(CShowProgram_Base::ProgramStep());

	unsigned uiContinueAfterMs = 0;

	while (IsEnabled() && m_nRepeatCounter < ARRAY_SIZE(g_aColorHues)) {
		if (m_nStepSub < SHOW_CHANNEL_COUNT) {
			unsigned nChannelOn = m_nStepSub % SHOW_CHANNEL_COUNT;
			unsigned nChannelOff = (m_nStepSub + SHOW_CHANNEL_COUNT - 1) % SHOW_CHANNEL_COUNT;
			float fColor = g_aColorHues[m_nRepeatCounter % ARRAY_SIZE(g_aColorHues)];
			float fOffColor = g_aColorHues[(m_nRepeatCounter + SHOW_CHANNEL_COUNT - 1) % ARRAY_SIZE(g_aColorHues)];
			m_aNozzle[nChannelOn].SetPump(1.0);
			m_aNozzle[nChannelOn].SetColor(make_color_hsv(fColor, 1.0, 0.2));
			m_aNozzle[nChannelOn].RampColor(make_color_hsv(fColor, 1.0, 1.0), 1.0);
			m_aNozzle[nChannelOff].SetPump(0.0);
			m_aNozzle[nChannelOff].RampColor(make_color_hsv(fOffColor, 1.0, 0.0), 1.0);
			++m_nStepSub;
			uiContinueAfterMs = 1000;
			break;
		} else {
			DoRepeat();
		}
	}

	return uiContinueAfterMs;
}
CShowProgram_FastShoots g_ShowProgram_FastShoots;

class CShowProgram_LongShoots:
	public CShowProgram_Base
{
public: // CShowProgram_Base
	virtual unsigned ProgramStep();
};
unsigned CShowProgram_LongShoots::ProgramStep()
{
	IMPLEMENTS_INTERFACE_METHOD(CShowProgram_Base::ProgramStep());

	unsigned uiContinueAfterMs = 0;

	while (IsEnabled() && m_nRepeatCounter < 5) {
		if (m_nStepSub < SHOW_CHANNEL_COUNT) {
			unsigned nChannelOn = m_nStepSub;
			unsigned nChannelOff = (m_nStepSub + SHOW_CHANNEL_COUNT - 1) % SHOW_CHANNEL_COUNT;
			m_aNozzle[nChannelOn].SetPump(1.0);
			m_aNozzle[nChannelOn].SetColor(make_color_hsv(0.0, 1.0, 1.0));
			m_aNozzle[nChannelOn].RampColor(make_color_hsv(1.0, 1.0, 1.0), 10.0);
			m_aNozzle[nChannelOff].SetPump(0.0);
			m_aNozzle[nChannelOff].RampColor(make_color_hsv(1.0, 1.0, 0.0), 1.0);
			++m_nStepSub;
			uiContinueAfterMs = 10000;
			break;
		} else {
			DoRepeat();
		}
	}

	return uiContinueAfterMs;
}
CShowProgram_LongShoots g_ShowProgram_LongShoots;

class CShowProgram_RandShoots:
	public CShowProgram_Base,
	public TRandSource<uint16_t>
{
public: // CShowProgram_Base
	virtual unsigned ProgramStep();
private:
	unsigned m_nActiveChannel;
	float m_fActiveColor;
};
unsigned CShowProgram_RandShoots::ProgramStep()
{
	IMPLEMENTS_INTERFACE_METHOD(CShowProgram_Base::ProgramStep());

	unsigned uiContinueAfterMs = 0;

	while (IsEnabled() && m_nRepeatCounter < 5) {
		if (m_nStepSub < SHOW_CHANNEL_COUNT * 2) {
			bool bPause = m_nStepSub % 2;
			m_aNozzle[m_nActiveChannel].SetPump(0.0);
			m_aNozzle[m_nActiveChannel].RampColor(make_color_hsv(m_fActiveColor, 1.0, 0.0), 1.0);
			if (!bPause) {
			} else {
				m_nActiveChannel = GetRand(SHOW_CHANNEL_COUNT);
				m_fActiveColor = GetRandF(1.0);
				m_aNozzle[m_nActiveChannel].SetPump(1.0);
				m_aNozzle[m_nActiveChannel].SetColor(make_color_hsv(m_fActiveColor, 1.0, 0.0));
				m_aNozzle[m_nActiveChannel].RampColor(make_color_hsv(m_fActiveColor, 1.0, 1.0), 0.1);
			}
			++m_nStepSub;
			uiContinueAfterMs = 1000;
			break;
		} else {
			DoRepeat();
		}
	}

	return uiContinueAfterMs;
}
CShowProgram_RandShoots g_ShowProgram_RandShoots;

class CShowProgram_Wave:
	public CShowProgram_Base
{
public: // CShowProgram_Base
	virtual unsigned ProgramStep();
};

unsigned CShowProgram_Wave::ProgramStep()
{
	IMPLEMENTS_INTERFACE_METHOD(CShowProgram_Base::ProgramStep());

	unsigned uiContinueAfterMs = 0;

	while (IsEnabled() && m_nRepeatCounter < 10) {
		if (m_nStepSub < SHOW_CHANNEL_COUNT) {
			unsigned nChannelOn = m_nStepSub;
			unsigned nChannelTop = (m_nStepSub + SHOW_CHANNEL_COUNT - 1) % SHOW_CHANNEL_COUNT;
			unsigned nChannelDone = (m_nStepSub + SHOW_CHANNEL_COUNT - 2) % SHOW_CHANNEL_COUNT;
			m_aNozzle[nChannelOn].RampPump(1.0, 5.0);
			m_aNozzle[nChannelOn].SetColor(make_color_hsv(0.0, 1.0, 1.0));
			m_aNozzle[nChannelOn].RampColor(make_color_hsv(1.0, 1.0, 1.0), 5.0);
			m_aNozzle[nChannelTop].RampPump(0.0, 5.0);
			m_aNozzle[nChannelTop].RampColor(make_color_hsv(0.0, 1.0, 1.0), 5.0);
			m_aNozzle[nChannelDone].SetColor(make_color_hsv(0.0, 0.0, 0.0));
			++m_nStepSub;
			uiContinueAfterMs = 5000;
			break;
		} else {
			DoRepeat();
		}
	}

	return uiContinueAfterMs;
}
CShowProgram_Wave g_ShowProgram_Wave;

class CShowProgram_WaveWide:
	public CShowProgram_Base
{
public: // CShowProgram_Base
	virtual unsigned ProgramStep();
};

unsigned CShowProgram_WaveWide::ProgramStep()
{
	IMPLEMENTS_INTERFACE_METHOD(CShowProgram_Base::ProgramStep());

	unsigned uiContinueAfterMs = 0;

	while (IsEnabled() && m_nRepeatCounter < 10) {
		if (m_nStepSub < SHOW_CHANNEL_COUNT) {
			unsigned nChannelOn = m_nStepSub;
			unsigned nChannelTop = (m_nStepSub + SHOW_CHANNEL_COUNT - 2) % SHOW_CHANNEL_COUNT;
			unsigned nChannelOff = (m_nStepSub + SHOW_CHANNEL_COUNT - 4) % SHOW_CHANNEL_COUNT;
			unsigned nChannelDone = (m_nStepSub + SHOW_CHANNEL_COUNT - 6) % SHOW_CHANNEL_COUNT;
			m_aNozzle[nChannelOn].RampPump(1.0, 5.0);
			m_aNozzle[nChannelOn].SetColor(make_color_hsv(0.0, 1.0, 1.0));
			m_aNozzle[nChannelOn].RampColor(make_color_hsv(0.833, 1.0, 1.0), 5.0);
			m_aNozzle[nChannelTop].SetColor(make_color_hsv(0.833, 0.0, 1.0));
			m_aNozzle[nChannelOff].RampPump(0.0, 5.0);
			m_aNozzle[nChannelOff].SetColor(make_color_hsv(0.833, 1.0, 1.0));
			m_aNozzle[nChannelOff].RampColor(make_color_hsv(0.0, 1.0, 1.0), 5.0);
			m_aNozzle[nChannelDone].SetColor(make_color_hsv(0.0, 0.0, 0.0));
			++m_nStepSub;
			uiContinueAfterMs = 2500;
			break;
		} else {
			DoRepeat();
		}
	}

	return uiContinueAfterMs;
}
CShowProgram_WaveWide g_ShowProgram_WaveWide;

CShowProgram_Base* g_apPrograms[] = {
		&g_ShowProgram_AllTogeza,
		&g_ShowProgram_FastShoots,
		&g_ShowProgram_WaveWide,
		&g_ShowProgram_RandShoots,
		&g_ShowProgram_Wave,
		&g_ShowProgram_LongShoots,
};


class CShowProcess :
	public IShow,
	public CTickProcess
{
	typedef CTickProcess CParentProcess;
public:
	void Init(
			CRampPump* apPumpRamps[],
			CRampLeds* apLightRamps[],
			ILed*      apLeds[],
			IKey<ticktime_t>* apKeys[],
			ITextDisplayDeviceMonochrome* pDisplay
	);
public: // IPlayer
	virtual void Play();
	virtual void Stop();
	virtual bool IsPlaying() const;
	virtual void SetPlayerNotificationListener(IPlayerNotificationListener* pListener);
public: // IPlaylist
	virtual bool PlayNext();
	virtual bool PlayPrev();
	virtual bool PlayN(unsigned nProgram);
	virtual bool PlayItem(IPlaylistEntry* pEntry);
	virtual unsigned GetItemsCount() const;
	virtual unsigned GetCurrentItem() const;
	virtual void SetPlaylistNotificationListener(IPlaylistNotificationListener* pListener);

public: // CTickProcess
	virtual void SingleStep(CTimeType ttTime);

private:
	unsigned FindItem(IPlaylistEntry* pEntry);

	unsigned m_nActiveProgram;
	IPlayerNotificationListener* m_pPlayerNotificationListener;
	IPlaylistNotificationListener* m_pPlaylistNotificationListener;
	ITextDisplayDeviceMonochrome* m_pDisplay;
};

void CShowProcess::Init(
		CRampPump* apPumpRamps[],
		CRampLeds* apLightRamps[],
		ILed*      apLeds[],
		IKey<ticktime_t>* apKeys[],
		ITextDisplayDeviceMonochrome* pDisplay
) {
	CParentProcess::Init(&g_TickProcessScheduller);

	m_nActiveProgram = 0;
	m_pPlayerNotificationListener = NULL;
	m_pPlaylistNotificationListener = NULL;
	m_pDisplay = pDisplay;
	unsigned nLed = 0;
	unsigned nKey = 0;

	for (unsigned n = 0; n < ARRAY_SIZE(g_apPrograms); ++n) {
		CShowProgram_Base* pProgram = g_apPrograms[n];
		ILed* pLed = apLeds[nLed];
		if (pLed) ++nLed;
		IKey<ticktime_t>* pKey = apKeys[nKey];
		if (pKey) ++nKey;
		pProgram->Init(this, apPumpRamps, apLightRamps, pLed, pKey, pDisplay);
	}
}

void CShowProcess::Play()
{
	IMPLEMENTS_INTERFACE_METHOD(IPlayer::Play());

	if (m_nActiveProgram < ARRAY_SIZE(g_apPrograms) && !IsPlaying()) {
		if (m_pPlayerNotificationListener) m_pPlayerNotificationListener->OnPlayStarts();
		CShowProgram_Base* pProgram = g_apPrograms[m_nActiveProgram];
		ASSERTE(pProgram);
		if (pProgram->IsEnabled()) {
			pProgram->Play();
			if (m_pDisplay) {
				m_pDisplay->PutString("program #", true);
				m_pDisplay->PutString(ToString<>(m_nActiveProgram + 1).GetValue(), true);
				m_pDisplay->PutString(" ", true);
			}
			ContinueAsap();
		}
	}
}
void CShowProcess::Stop()
{
	IMPLEMENTS_INTERFACE_METHOD(IPlayer::Stop());
	if (m_nActiveProgram < ARRAY_SIZE(g_apPrograms) && IsPlaying()) {
		CShowProgram_Base* pProgram = g_apPrograms[m_nActiveProgram];
		ASSERTE(pProgram);
		if (pProgram->IsPlaying()) {
			pProgram->Stop();
			if(m_pDisplay) m_pDisplay->PutString("=\n", true);
		}
		Cancel();
		if (m_pPlayerNotificationListener) m_pPlayerNotificationListener->OnPlayStops();
	}
}

bool CShowProcess::IsPlaying() const
{
	IMPLEMENTS_INTERFACE_METHOD(IPlayer::IsPlaying());
	if (m_nActiveProgram < ARRAY_SIZE(g_apPrograms)) {
		CShowProgram_Base* pProgram = g_apPrograms[m_nActiveProgram];
		return pProgram->IsPlaying();
	}
	return false;
}

void CShowProcess::SetPlayerNotificationListener(IPlayerNotificationListener* pListener)
{
	IMPLEMENTS_INTERFACE_METHOD(IPlaylist::SetPlayerNotificationListener(pListener));
	m_pPlayerNotificationListener = pListener;
}

bool CShowProcess::PlayNext()
{
	IMPLEMENTS_INTERFACE_METHOD(IPlaylist::PlayNext());

	if(m_pDisplay) m_pDisplay->PutString(">\n", true);

	for (unsigned n = 1; n < ARRAY_SIZE(g_apPrograms); ++n) {
		unsigned nProgram = (m_nActiveProgram + n) % ARRAY_SIZE(g_apPrograms);
		if (PlayN(nProgram)) {
			return true;
		}
	}
	return false;
}

bool CShowProcess::PlayPrev()
{
	IMPLEMENTS_INTERFACE_METHOD(IPlaylist::PlayPrev());

	if(m_pDisplay) m_pDisplay->PutString("<\n", true);

	for (unsigned n = 1; n < ARRAY_SIZE(g_apPrograms); ++n) {
		unsigned nProgram = (m_nActiveProgram + ARRAY_SIZE(g_apPrograms) - n) % ARRAY_SIZE(g_apPrograms);
		if (PlayN(nProgram)) {
			return true;
		}
	}
	return false;
}

unsigned CShowProcess::FindItem(IPlaylistEntry* pEntry)
{
	for (unsigned n = 0; n < ARRAY_SIZE(g_apPrograms); ++n) {
		CShowProgram_Base* pProgram = g_apPrograms[n];
		if (pProgram == pEntry) {
			return n;
		}
	}
	return ARRAY_SIZE(g_apPrograms);
}

bool CShowProcess::PlayN(unsigned nProgram)
{
	IMPLEMENTS_INTERFACE_METHOD(IPlaylist::PlayN(nProgram));

	if (m_nActiveProgram != nProgram) {
		CShowProgram_Base* pProgram = g_apPrograms[m_nActiveProgram];
		ASSERTE(pProgram);
		pProgram->Stop();
	}
	if (nProgram < ARRAY_SIZE(g_apPrograms)) {
		CShowProgram_Base* pProgram = g_apPrograms[nProgram];
		ASSERTE(pProgram);
		if (pProgram->IsEnabled()) {
			m_nActiveProgram = nProgram;
			pProgram->Play();
			if (m_pDisplay) {
				m_pDisplay->PutString("program #", true);
				m_pDisplay->PutString(ToString<>(m_nActiveProgram + 1).GetValue(), true);
				m_pDisplay->PutString(" ", true);
			}
			ContinueAsap();
			return true;
		}
	}
	Cancel();
	return false;
}

bool CShowProcess::PlayItem(IPlaylistEntry* pEntry)
{
	IMPLEMENTS_INTERFACE_METHOD(IPlaylist::PlayItem(pEntry));

	unsigned n = FindItem(pEntry);
	if (n < ARRAY_SIZE(g_apPrograms)) {
		return PlayN(n);
	}
	return false;
}
unsigned CShowProcess::GetItemsCount() const
{
	IMPLEMENTS_INTERFACE_METHOD(IPlaylist::GetItemsCount());

	return ARRAY_SIZE(g_apPrograms);
}
unsigned CShowProcess::GetCurrentItem() const
{
	IMPLEMENTS_INTERFACE_METHOD(IPlaylist::GetCurrentItem());

	return m_nActiveProgram;
}

void CShowProcess::SetPlaylistNotificationListener(IPlaylistNotificationListener* pListener)
{
	IMPLEMENTS_INTERFACE_METHOD(IPlaylist::SetPlaylistNotificationListener(pListener));
	m_pPlaylistNotificationListener = pListener;
}


void CShowProcess::SingleStep(CTimeType ttTime)
{
	IMPLEMENTS_INTERFACE_METHOD(CParentProcess::SingleStep(ttTime));
	PROCESS_DEBUG_INFO(">>> Process: CShowProcess");

	CShowProgram_Base* pProgram = g_apPrograms[m_nActiveProgram];
	unsigned uiContinueAfterMs = pProgram->ProgramStep();
	if (uiContinueAfterMs) {
		ContinueAt(ttTime + uiContinueAfterMs);
	} else {
		PlayNext();
	}

	PROCESS_DEBUG_INFO("<<< Process: CShowProcess");
}

CShowProcess g_ShowProcess;
IShow* AcquireShowInterface(
		CRampPump* apPumpRamps[],
		CRampLeds* apLightRamps[],
		ILed* apLeds[],
		IKey<ticktime_t>* apKeys[],
		ITextDisplayDeviceMonochrome* pDisplay
) {
	g_ShowProcess.Init(apPumpRamps, apLightRamps, apLeds, apKeys, pDisplay); return &g_ShowProcess;
};
