#include "stdafx.h"
#include "hw/hw.h"
#include "util/macros.h"
#include "util/utils.h"
#include "util/map.h"
#include "piped_uart.h"
#include "tick_process.h"
#include "proc_show.h"
#include "proc_ramp.h"
#include "color.h"
#include "transformer.h"
#ifdef USE_TRIAC
#  include "zero_sync_pwm.h"
#endif
#ifdef USE_KEYSCANNER
#  include "proc_keyscanner.h"
#endif
#ifdef USE_MODBUS
#  include "proc_modbus.h"
#  include "modbus_slave.h"
#endif
#ifdef USE_PARAMS
#  include "modules/params/params.h"
#endif
#ifdef USE_TERMINAL
#  include "fountain_console.h"
#endif
#include "proc_softpwm.h"
#include <stdio.h>
#include <stdint.h>

#define LIGHT_PWM_FREQ 100
#define NOZZLES_COUNT 7
#define FILTER_PUMP_TRIAC_CHANNEL 7 // TRIAC8
#define DC24_CONTROL_CHANNEL PU8

static pair<float, float> g_PumpPowerTransformation = {0.5, 0.0};

#define FnH(X) (1.0*X/240.0)
static pair<float, float> g_aPumpCalibrationTransformations[7][11] = {
{ // Pump 1
		{FnH(0  ),  0.27}, //0
		{FnH(5  ),  0.28}, //5
		{FnH(40 ),  0.30}, //40
		{FnH(70 ),  0.33}, //70
		{FnH(80 ),  0.35}, //80
		{FnH(125),  0.40}, //125
		{FnH(185),  0.45}, //185
		{FnH(215),  0.50}, //215
		{FnH(235),  0.60}, //235
		{FnH(255),  0.70}, //255
		{FnH(270),  1.00}, //270
}, { // Pump 2
		{FnH(0  ),  0.28}, //0
		{FnH(5  ),  0.29}, //5
		{FnH(20 ),  0.30}, //20
		{FnH(55 ),  0.33}, //55
		{FnH(65 ),  0.35}, //65
		{FnH(110),  0.40}, //110
		{FnH(150),  0.45}, //150
		{FnH(190),  0.50}, //190
		{FnH(225),  0.60}, //225
		{FnH(245),  0.70}, //245
		{FnH(275),  1.00}, //275
}, { // Pump 3
		{FnH(0  ),  0.27}, //0
		{FnH(5  ),  0.28}, //5
		{FnH(10 ),  0.30}, //10
		{FnH(45 ),  0.33}, //45
		{FnH(55 ),  0.35}, //55
		{FnH(110),  0.40}, //110
		{FnH(155),  0.45}, //155
		{FnH(175),  0.50}, //175
		{FnH(220),  0.60}, //220
		{FnH(230),  0.70}, //230
		{FnH(240),  1.00}, //240
}, { // Pump 4
		{FnH(0  ),  0.27}, //0
		{FnH(5  ),  0.28}, //5
		{FnH(25 ),  0.30}, //25
		{FnH(60 ),  0.33}, //60
		{FnH(75 ),  0.35}, //75
		{FnH(115),  0.40}, //115
		{FnH(165),  0.45}, //165
		{FnH(195),  0.50}, //195
		{FnH(235),  0.60}, //235
		{FnH(250),  0.70}, //250
		{FnH(270),  1.00}, //270
}, { // Pump 5
		{FnH(0  ),  0.24}, //0
		{FnH(10 ),  0.26}, //10
		{FnH(40 ),  0.30}, //40
		{FnH(70 ),  0.33}, //70
		{FnH(80 ),  0.35}, //80
		{FnH(130),  0.40}, //130
		{FnH(175),  0.45}, //175
		{FnH(230),  0.50}, //230
		{FnH(255),  0.60}, //255
		{FnH(300),  0.70}, //300
		{FnH(315),  1.00}, //315
}, { // Pump  6
		{FnH(0  ),  0.25}, //0
		{FnH(20 ),  0.29}, //20
		{FnH(35 ),  0.30}, //35
		{FnH(50 ),  0.33}, //50
		{FnH(75 ),  0.35}, //75
		{FnH(125),  0.40}, //125
		{FnH(170),  0.45}, //170
		{FnH(210),  0.50}, //210
		{FnH(245),  0.60}, //245
		{FnH(260),  0.70}, //260
		{FnH(270),  1.00}, //270
}, { // Pump 7
		{FnH(0  ),  0.27}, //0
		{FnH(15 ),  0.30}, //15
		{FnH(35 ),  0.33}, //35
		{FnH(50 ),  0.35}, //50
		{FnH(80 ),  0.37}, //80
		{FnH(115),  0.40}, //115
		{FnH(145),  0.45}, //145
		{FnH(180),  0.50}, //180
		{FnH(215),  0.60}, //215
		{FnH(235),  0.70}, //235
		{FnH(240),  1.00}, //240
}};

typedef ITimer* (*CAcquireTimerFn)();
static CAcquireTimerFn g_afnLedPwmTimersFactories[] = {
		&CHW_Brd::AcquireTimerInterface_1,
		&CHW_Brd::AcquireTimerInterface_2,
		&CHW_Brd::AcquireTimerInterface_8,
		&CHW_Brd::AcquireTimerInterface_9,
		&CHW_Brd::AcquireTimerInterface_10,
		&CHW_Brd::AcquireTimerInterface_11,
};

ITimerChannel* AcquireLedPwmTimerChannelInterface(unsigned uiNozzleIndex, unsigned uiColorIndex, ITimerSoft* pSoftTimer) {
	unsigned uiIndex = uiNozzleIndex * 3 + uiColorIndex;
	switch (uiIndex) {
	// Nozzle 1
	case 0:  return pSoftTimer->AcquireChannelInterface(CHW_Brd::AcquireDigitalOutInterface_DIO1(false), 0, true);
	case 1:  return pSoftTimer->AcquireChannelInterface(CHW_Brd::AcquireDigitalOutInterface_DIO2(false), 0, true);
	case 2:  return pSoftTimer->AcquireChannelInterface(CHW_Brd::AcquireDigitalOutInterface_DIO3(false), 0, true);
	// Nozzle 2
	case 3:  return pSoftTimer->AcquireChannelInterface(CHW_Brd::AcquireDigitalOutInterface_DIO4(false), 0, true);
	case 4:  return pSoftTimer->AcquireChannelInterface(CHW_Brd::AcquireDigitalOutInterface_DIO5(false), 0, true);
	case 5:  return pSoftTimer->AcquireChannelInterface(CHW_Brd::AcquireDigitalOutInterface_DIO6(false), 0, true);
	// Nozzle 3
	case 6:  return CHW_Brd::AcquireTimerChannelInterface_DOUT1(0, true);
	case 7:  return CHW_Brd::AcquireTimerChannelInterface_DOUT2(0, true);
	case 8:  return CHW_Brd::AcquireTimerChannelInterface_DOUT3(0, true);
	// Nozzle 4
	case 9:  return CHW_Brd::AcquireTimerChannelInterface_DOUT4(0, true);
	case 10: return CHW_Brd::AcquireTimerChannelInterface_DOUT5(0, true);
	case 11: return CHW_Brd::AcquireTimerChannelInterface_DOUT6(0, true);
	// Nozzle 5
	case 12: return CHW_Brd::AcquireTimerChannelInterface_EXT1(0, true);
	case 13: return CHW_Brd::AcquireTimerChannelInterface_EXT2(0, true);
	case 14: return CHW_Brd::AcquireTimerChannelInterface_EXT3(0, true);
	// Nozzle 6
	case 15: return CHW_Brd::AcquireTimerChannelInterface_EXT4(0, true);
	case 16: return CHW_Brd::AcquireTimerChannelInterface_PWM1(0, true);
	case 17: return CHW_Brd::AcquireTimerChannelInterface_PWM2(0, true);
	// Nozzle 7
	case 18: return CHW_Brd::AcquireTimerChannelInterface_PWM3(0, true);
	case 19: return CHW_Brd::AcquireTimerChannelInterface_PWM4(0, true);
	case 20: return pSoftTimer->AcquireChannelInterface(CHW_Brd::AcquireDigitalOutInterface_DOUT7(false), 0, true);
	}
	return NULL;
}

ITriacChannel<float>* g_pFilterPumpChannel = NULL;
void FilterPumpControl(bool bEnabled)
{
	if (g_pFilterPumpChannel) g_pFilterPumpChannel->SetValue(bEnabled ? 1.0 : 0.0);

}

ILed* g_pDc24Led = NULL;
IDigitalOut *g_pDc24DOut = NULL;
bool IsDc24Enabled() {return !g_pDc24DOut || g_pDc24DOut->GetValue();}
void Dc24Control(bool bEnabled) {
	if (g_pDc24DOut) g_pDc24DOut->SetValue(bEnabled);
	if (g_pDc24Led) {
		if (bEnabled) {
			g_pDc24Led->Set();
		} else {
			g_pDc24Led->Clear();
		}
	}
}


#ifdef USE_KEYSCANNER
class CShowBaseKeyHandler:
		public IKeyHandler<ticktime_t>
{
public: // IKeyHandler<ticktime_t>
	virtual void OnButtonRelease(ticktime_t ttTime);
	virtual void OnButtonHold(ticktime_t ttTime);
};
void CShowBaseKeyHandler::OnButtonHold(ticktime_t ttTime) { IMPLEMENTS_INTERFACE_METHOD(IKeyHandler<ticktime_t>::OnButtonHold(ttTime));};
void CShowBaseKeyHandler::OnButtonRelease(ticktime_t ttTime) { IMPLEMENTS_INTERFACE_METHOD(IKeyHandler<ticktime_t>::OnButtonRelease(ttTime)); };

static IShow* g_pShow;

class CShowPlayPauseKeyHandler: public CShowBaseKeyHandler
{
public: // IKeyHandler<ticktime_t>
	virtual void OnButtonPress(ticktime_t ttTime) {
		IMPLEMENTS_INTERFACE_METHOD(IKeyHandler<ticktime_t>::OnButtonPress(ttTime));
		if (g_pShow->IsPlaying()) {
			g_pShow->Stop();
		} else {
			g_pShow->Play();
		}
	};
} g_ShowPlayPauseKeyHandler;

class CShowLightOnOffKeyHandler: public CShowBaseKeyHandler
{
public: // IKeyHandler<ticktime_t>
	virtual void OnButtonPress(ticktime_t ttTime) {
		IMPLEMENTS_INTERFACE_METHOD(IKeyHandler<ticktime_t>::OnButtonPress(ttTime));
		Dc24Control(!IsDc24Enabled());
	};
} g_ShowLightOnOffKeyHandler;

class CShowNextKeyHandler: public CShowBaseKeyHandler
{
public: // IKeyHandler<ticktime_t>
	virtual void OnButtonPress(ticktime_t ttTime) {
		IMPLEMENTS_INTERFACE_METHOD(IKeyHandler<ticktime_t>::OnButtonPress(ttTime));
		g_pShow->PlayNext();
	};
} g_ShowNextKeyHandler;

class CShowPrevKeyHandler: public CShowBaseKeyHandler
{
public: // IKeyHandler<ticktime_t>
	virtual void OnButtonPress(ticktime_t ttTime) {
		IMPLEMENTS_INTERFACE_METHOD(IKeyHandler<ticktime_t>::OnButtonPress(ttTime));
		g_pShow->PlayPrev();
	};
} g_ShowPrevKeyHandler;

class CPumpsMaxPowerKeyHandler:
		public CShowBaseKeyHandler
{
public: // CPumpsMaxPowerKeyHandler
	void ChangePumpPower(float fPowerDelta);
};
void CPumpsMaxPowerKeyHandler::ChangePumpPower(float fPowerDelta) {
	float& fPower = g_PumpPowerTransformation.first;
	fPower = LIMIT(fPower * fPowerDelta, 0.05, 1.0);
}

class CDecreasePumpsMaxPowerKeyHandler:
		public CPumpsMaxPowerKeyHandler
{
public: // IKeyHandler<ticktime_t>
	virtual void OnButtonPress(ticktime_t ttTime);
};
void CDecreasePumpsMaxPowerKeyHandler::OnButtonPress(ticktime_t ttTime)
{
	IMPLEMENTS_INTERFACE_METHOD(IKeyHandler<ticktime_t>::OnButtonPress(ttTime));
	ChangePumpPower(1/1.1);
}
CDecreasePumpsMaxPowerKeyHandler g_DecreasePumpsMaxPowerKeyHandler;

class CIncreasePumpsMaxPowerKeyHandler:
		public CPumpsMaxPowerKeyHandler
{
public: // IKeyHandler<ticktime_t>
	virtual void OnButtonPress(ticktime_t ttTime);
};
void CIncreasePumpsMaxPowerKeyHandler::OnButtonPress(ticktime_t ttTime)
{
	IMPLEMENTS_INTERFACE_METHOD(IKeyHandler<ticktime_t>::OnButtonPress(ttTime));
	ChangePumpPower(1.1);
}
CIncreasePumpsMaxPowerKeyHandler g_IncreasePumpsMaxPowerKeyHandler;

#endif // USE_KEYSCANNER


class CPwmChannel:
	public IValueReceiver<float>,   // command value
	public IValueRequester<float>  // command value
{
public:
	void Init(ITimerChannel* pTimerChannel) {
		m_pTimerChannel = pTimerChannel;
		m_uiMultiplier = m_pTimerChannel?m_pTimerChannel->GetTimerPeriod():1;
	}
public: // IValueReceiver<float>
	virtual void SetValue(float tValue);
public: // IValueRequester
	virtual void SetValueSourceInterface(const IValueSource<float>* pValueSource);
private:
	ITimerChannel* m_pTimerChannel;
	unsigned       m_uiMultiplier;
	const IValueSource<float>* m_pValueSource;
};
/*virtual*/
void CPwmChannel::SetValue(float tValue) {
	IMPLEMENTS_INTERFACE_METHOD(IValueReceiver<float>::SetValue(tValue));
	if (m_pTimerChannel) m_pTimerChannel->SetValue(m_uiMultiplier * tValue);
}
/*virtual*/
void CPwmChannel::SetValueSourceInterface(const IValueSource<float>* pValueSource) {
	IMPLEMENTS_INTERFACE_METHOD(IValueRequester<float>::SetValueSourceInterface(pValueSource));
	m_pValueSource = pValueSource;
}
CPwmChannel g_aCPwmChannel[NOZZLES_COUNT * 3];
CPwmChannel* GetPwmChannelInterface(unsigned uiNozzleIndex, unsigned uiColorIndex) {
	unsigned uiIndex = uiNozzleIndex * 3 + uiColorIndex;
	return (uiIndex < ARRAY_SIZE(g_aCPwmChannel)) ? &g_aCPwmChannel[uiIndex] : NULL;
}


class CRgbSplitter:
		public IValueReceiver< CColorRGB >
{
public:
	virtual void Init(IValueReceiver<float>* pLedRTargetChannel, IValueReceiver<float>* pLedGTargetChannel, IValueReceiver<float>* pLedBTargetChannel) {
		ASSERTE(pLedRTargetChannel);
		ASSERTE(pLedGTargetChannel);
		ASSERTE(pLedBTargetChannel);
		m_pLedRTargetChannel = pLedRTargetChannel;
		m_pLedGTargetChannel = pLedGTargetChannel;
		m_pLedBTargetChannel = pLedBTargetChannel;
	}
public: // IValueTarget
	virtual void SetValue(CColorRGB tValue) {
		m_pLedRTargetChannel->SetValue(tValue.first);
		m_pLedGTargetChannel->SetValue(tValue.second);
		m_pLedBTargetChannel->SetValue(tValue.third);
	}
private:
	IValueReceiver<float>* m_pLedRTargetChannel;
	IValueReceiver<float>* m_pLedGTargetChannel;
	IValueReceiver<float>* m_pLedBTargetChannel;
};
CRgbSplitter g_aRgbSplitters[NOZZLES_COUNT];
CRgbSplitter* GetRgbSplitterinterface(unsigned n) {
	return (n < ARRAY_SIZE(g_aRgbSplitters)) ? &g_aRgbSplitters[n] : NULL;
}

CRampPump*  g_apPumpRamps[NOZZLES_COUNT];
ITransformer<float>* g_apPumpCalibrationTransformers[NOZZLES_COUNT];
CRampLeds* g_apLightRamps[NOZZLES_COUNT];

class CChannelsAttach :
		public IPlayerNotificationListener
{
public: // IPlayerNotificationListener
	virtual void OnPlayStarts() {
		ITriacZeroSyncPWM<float>* pTriacZeroSyncPWM = GetTriacZeroSyncPWMInterface();
		for (unsigned n = 0; n < NOZZLES_COUNT; ++n) {
			ITriacChannel<float>* pTriac = pTriacZeroSyncPWM->GetTriacChannelInterface(n);
			pTriac->SetBoostParams(4, 100, 5);
			pTriac->SetInterleaveParams(GET_PARAM_VALUE(TRIAC_INTERLEAVE), GET_PARAM_VALUE(TRIAC_INTERLEAVE_TRESHOLD));
			pTriac->SetCalibrationTransformer(g_apPumpCalibrationTransformers[n]);
			pTriac->SetValueSourceInterface(g_apPumpRamps[n]);
		}
	}
	virtual void OnPlayStops() {
		ITriacZeroSyncPWM<float>* pTriacZeroSyncPWM = GetTriacZeroSyncPWMInterface();
		for (unsigned n = 0; n < NOZZLES_COUNT; ++n) {
			ITriacChannel<float>* pTriac = pTriacZeroSyncPWM->GetTriacChannelInterface(n);
			pTriac->SetBoostParams(0, 0, 0);
			pTriac->SetInterleaveParams(0, 0);
			pTriac->SetCalibrationTransformer(NULL);
			pTriac->SetValueSourceInterface(NULL);
			pTriac->SetValue(0);
		}
	}
} g_ChannelsAttacher;

TPipedSerialRxAdapter<128,true> g_VCPRxPipeAdapter;
TPipedSerialTxAdapter<200>      g_VCPTxPipeAdapter;

extern "C" {

void app_init(void)
{
	CHW_Brd::AcquireDigitalOutInterface_PU1(true);
	CHW_Brd::AcquireDigitalOutInterface_PU2(true);
	CHW_Brd::AcquireDigitalOutInterface_PU3(true);
	CHW_Brd::AcquireDigitalOutInterface_PU4(true);
	CHW_Brd::AcquireDigitalOutInterface_PU5(true);
	CHW_Brd::AcquireDigitalOutInterface_PU6(true);
	CHW_Brd::AcquireDigitalOutInterface_PU7(true);
	CHW_Brd::AcquireDigitalOutInterface_PU8(false);

	ITransformer<float>* pPumpsPowerTransformer = AcquireOffsetScaleTransformer(&g_PumpPowerTransformation);
	ITriacZeroSyncPWM<float>* pTriacZeroSyncPWM = GetTriacZeroSyncPWMInterface();
	for (unsigned n = 0; n < NOZZLES_COUNT; ++n) {
		g_apPumpRamps[n] = AcquireRampProcess(0.005, 20, pPumpsPowerTransformer);
		g_apPumpCalibrationTransformers[n] = AcquireLinearTransformer(
				g_aPumpCalibrationTransformations[n],
				ARRAY_SIZE(g_aPumpCalibrationTransformations[0]));
	}

	g_pFilterPumpChannel = pTriacZeroSyncPWM->GetTriacChannelInterface(FILTER_PUMP_TRIAC_CHANNEL);
	g_pDc24DOut = CHW_Brd::CC(AcquireDigitalOutInterface_,DC24_CONTROL_CHANNEL)(false);
#ifdef USE_KEYSCANNER
	g_pDc24Led = GetLedsInterface()->AcquireLedInterface(6);
#endif // USE_KEYSCANNER

	unsigned uiLightPwmPeriodClk = CHW_Clocks::GetCLKFrequency() / LIGHT_PWM_FREQ;
	for (unsigned n = 0; n < ARRAY_SIZE(g_afnLedPwmTimersFactories); ++n) {
		CAcquireTimerFn fnA = g_afnLedPwmTimersFactories[n];
		ITimer* pTimer = fnA();
		pTimer->SetPeriodClks(uiLightPwmPeriodClk);
		pTimer->Start();
	}
	ITimerSoft* pSoftTimer = AcquireSoftPwmTimer();
	pSoftTimer->SetPeriodClks(uiLightPwmPeriodClk);
	pSoftTimer->Start();

	const ITransformer< CColorHSV, CColorRGB >* pHsvToRgbTransformer = GetHsvToRgbTransformer();
	for (unsigned n = 0; n < NOZZLES_COUNT; ++n) {
		g_apLightRamps[n] = AcquireRamp3Process(make_color_hsv(0.005, 0.005, 0.005), 20, pHsvToRgbTransformer);
		CPwmChannel* pLedRChannel = GetPwmChannelInterface(n, 0);
		pLedRChannel->Init(AcquireLedPwmTimerChannelInterface(n, 0, pSoftTimer));
		CPwmChannel* pLedGChannel = GetPwmChannelInterface(n, 1);
		pLedGChannel->Init(AcquireLedPwmTimerChannelInterface(n, 1, pSoftTimer));
		CPwmChannel* pLedBChannel = GetPwmChannelInterface(n, 2);
		pLedBChannel->Init(AcquireLedPwmTimerChannelInterface(n, 2, pSoftTimer));
		CRgbSplitter* pRgbSplitter = GetRgbSplitterinterface(n);
		pRgbSplitter->Init(pLedRChannel, pLedGChannel, pLedBChannel);
		g_apLightRamps[n]->SetValueTargetInterface(pRgbSplitter);
	}

	FilterPumpControl(true);

	ILed* apLeds[] = {
#ifdef USE_KEYSCANNER
			GetLedsInterface()->AcquireLedInterface(0),
			GetLedsInterface()->AcquireLedInterface(1),
			GetLedsInterface()->AcquireLedInterface(2),
			GetLedsInterface()->AcquireLedInterface(3),
			GetLedsInterface()->AcquireLedInterface(4),
			GetLedsInterface()->AcquireLedInterface(5),
#endif // USE_KEYSCANNER
			NULL
	};
	IKey<ticktime_t>* apKeys[] = {
#ifdef USE_KEYSCANNER
//			GetKeyboardInterface()->GetKeyInterface(0),
#endif // USE_KEYSCANNER
			NULL
	};
	ITextDisplayDeviceMonochrome* pDisplay = CHW_Brd::AcquireDisplayInterface();
	g_pShow = AcquireShowInterface(g_apPumpRamps, g_apLightRamps, apLeds, apKeys, pDisplay);
	g_pShow->SetPlayerNotificationListener(&g_ChannelsAttacher);
	g_pShow->Play();

#ifdef USE_KEYSCANNER
	GetKeyboardInterface()->GetKeyInterface(0)->AssignKeyHandler(&g_ShowPlayPauseKeyHandler);
	GetKeyboardInterface()->GetKeyInterface(1)->AssignKeyHandler(&g_ShowLightOnOffKeyHandler);

	GetKeyboardInterface()->GetKeyInterface(2)->AssignKeyHandler(&g_ShowNextKeyHandler);
	GetKeyboardInterface()->GetKeyInterface(3)->AssignKeyHandler(&g_ShowPrevKeyHandler);

	GetKeyboardInterface()->GetKeyInterface(6)->AssignKeyHandler(&g_IncreasePumpsMaxPowerKeyHandler);
	GetKeyboardInterface()->GetKeyInterface(7)->AssignKeyHandler(&g_DecreasePumpsMaxPowerKeyHandler);
#endif // USE_KEYSCANNER

#ifdef USE_MODBUS
//	GetModbusSlaveProcessor()->Init(0, InitDimmerModbusSlave(GET_PARAM_VALUE(DIMMER_MODBUS_SLAVE_ADDRESS)));
#endif // USE_MODBUS

#ifdef USE_TERMINAL
#ifdef USE_USB
	ISerial* pVcp = CHW_MCU::GetUSBSerialInterface(0);
//	ISerial* pUSART = CHW_Brd::GetInitedUSART(0);

	g_VCPTxPipeAdapter.Init(pVcp);
	CStreamFilter* pConsoleShell = AcquireConsoleShell();
	pConsoleShell->Bind(&g_VCPTxPipeAdapter);
	CConsoleContext* pConsoleContext = AcquireConsoleContext(g_pShow);
	pConsoleContext->BindStdout(&g_VCPTxPipeAdapter);
	pConsoleContext->SetCommandsMapRoot(AcquireConsoleCommandMapRoot());
	IStreamLineEdit* pStreamLineEdit = GetStreamLineEdit();
	pStreamLineEdit->Bind(&g_VCPTxPipeAdapter);
	pStreamLineEdit->BindLineProcessor(pConsoleShell);
	g_VCPRxPipeAdapter.Init(pVcp, pStreamLineEdit);
#endif
#endif

}

} // extern "C"
