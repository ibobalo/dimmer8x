#include "stdafx.h"

#include "terminal.h"
#include "console_command.h"
#include "console_context.h"

#include "hw/hw.h"
#include "zero_sync_pwm.h"
#include "proc_show.h"


class CFountainConsoleContext :
		public TConsoleContext<128>
{
public:
	void Init(IShow* pShow) {m_pShow = pShow;}
	IShow* GetShow() {return m_pShow;}
private:
	IShow* m_pShow;
};

CFountainConsoleContext g_ConsoleContext;
CConsoleContext* AcquireConsoleContext(IShow* pShow) {g_ConsoleContext.Init(pShow); return &g_ConsoleContext;}



#define g_ConsoleCommandMapItem_shell_NULL *((const CConsoleCommandMapItem*)0)
CONSOLE_COMMAND_DECLARATION(shell, help);
CONSOLE_COMMAND_DECLARATION(shell, reboot);
CONSOLE_COMMAND_DECLARATION(shell, show);
CONSOLE_COMMAND_DECLARATION(shell, triac);
/*              show
 *      reboot         triac
 * help       -      -       -
 */
CONSOLE_COMMAND_BTREE(shell, show,   reboot, NULL,   triac);
CONSOLE_COMMAND_BTREE(shell, help,   NULL,   reboot, NULL);
CONSOLE_COMMAND_BTREE(shell, reboot, help,   show,   NULL);
CONSOLE_COMMAND_BTREE(shell, triac,  NULL,   show,   NULL);
#define g_ConsoleCommandMapItem_shell_ROOT g_ConsoleCommandMapItem_shell_show

#define g_ConsoleCommandMapItem_show_NULL *((const CConsoleCommandMapItem*)0)
CONSOLE_COMMAND_DECLARATION(show, play);
CONSOLE_COMMAND_DECLARATION(show, stop);
CONSOLE_COMMAND_DECLARATION(show, next);
CONSOLE_COMMAND_DECLARATION(show, prev);
/*               prev
 *      play             stop
 * next       -       -       -
 */
CONSOLE_COMMAND_BTREE(show, prev, play, NULL, stop);
CONSOLE_COMMAND_BTREE(show, play, next, prev, NULL);
CONSOLE_COMMAND_BTREE(show, stop, NULL, prev, NULL);
CONSOLE_COMMAND_BTREE(show, next, NULL, play, NULL);
#define g_ConsoleCommandMapItem_show_ROOT g_ConsoleCommandMapItem_show_prev


CONSOLE_COMMAND_DEFINITION(shell, reboot,
		"reboot system"
	) {
		IMPLEMENTS_INTERFACE_METHOD(CConsoleCommandBase::Exec());
		CHW_MCU::Reboot();
	}

CONSOLE_COMMAND_DEFINITION(shell, help,
		"show usage info"
	) {
		IMPLEMENTS_INTERFACE_METHOD(CConsoleCommandBase::Exec());
		static STR_CHUNK(ccHelpText, "help text here\n\r");
		Stdout(ccHelpText);
	}

CONSOLE_COMMAND_DEFINITION(shell, triac,
		"show/change triac output power"
	) {
		IMPLEMENTS_INTERFACE_METHOD(CConsoleCommandBase::Exec());
		ITriacZeroSyncPWM<float>* pTriacsInterface = GetTriacZeroSyncPWMInterface();
		if (!pTriacsInterface) {
			static STR_CHUNK(cc, "triacs interface unavailable\r\n");
			Stdout(cc);
			return;
		}
		if (!HasNextParam()) {
			static STR_CHUNK(cc, "triac zero pulsing: ");
			Stdout(cc);
			StdoutU(pTriacsInterface->GetZeroQuality());
			StdoutEOL();
			return;
		}
		unsigned uiChannelIdx = GetNextParamU(0);
		ITriacChannel<float>* pTricChannelInterface;
		if (uiChannelIdx < 1 || !(pTricChannelInterface = pTriacsInterface->GetTriacChannelInterface(uiChannelIdx - 1))) {
			static STR_CHUNK(cc, "triacs channel unavailable\r\n");
			Stdout(cc);
			return;
		}
		if (!HasNextParam()) {
			static STR_CHUNK(cc, "value:");
			Stdout(cc);
			float fValue = pTricChannelInterface->GetValue();
			StdoutF(fValue);
			StdoutEOL();
			return;
		}
		float fValueToSet = GetNextParamF(0.0);
		pTricChannelInterface->SetValue(fValueToSet);
		static STR_CHUNK(cc, "new value:");
		Stdout(cc);
		StdoutF(fValueToSet);
		StdoutEOL();
	}

CONSOLE_COMMAND_DEFINITION(shell, show,
		"show player control"
	) {
		IMPLEMENTS_INTERFACE_METHOD(CConsoleCommandBase::Exec());
		IShow* pShow = g_ConsoleContext.GetShow();
		if (!pShow) {
			static STR_CHUNK(cc, "show controller interface unavailable\r\n");
			Stdout(cc);
			return;
		}
		if (HasNextParam()) {
			CConsoleSubContext SubContext;
			SubContext.Init(GetContext(), 2, &g_ConsoleCommandMapItem_show_ROOT);
			CConstChunk ccAction = GetNextParam();
			SubContext.Execute(ccAction);
		}
		static STR_CHUNK(ccPlaying, "playing:");
		static STR_CHUNK(ccPaused,  "paused:");
		Stdout(pShow->IsPlaying()?ccPlaying:ccPaused);
		unsigned uiItemIdx = pShow->GetCurrentItem();
		StdoutU(uiItemIdx);
		StdoutEOL();
		return;

	}

CONSOLE_COMMAND_DEFINITION(show, play,
		"start/resume show"
	) {
		IMPLEMENTS_INTERFACE_METHOD(CConsoleCommandBase::Exec());
		IShow* pShow = g_ConsoleContext.GetShow();
		if (!pShow) {
			static STR_CHUNK(cc, "show controller interface unavailable\r\n");
			Stdout(cc);
			return;
		}
		pShow->Play();
	}
CONSOLE_COMMAND_DEFINITION(show, stop,
		"stop/pause show"
	) {
		IMPLEMENTS_INTERFACE_METHOD(CConsoleCommandBase::Exec());
		IShow* pShow = g_ConsoleContext.GetShow();
		if (!pShow) {
			static STR_CHUNK(cc, "show controller interface unavailable\r\n");
			Stdout(cc);
			return;
		}
		if (pShow->IsPlaying()) {
			pShow->Stop();
		}
	}
CONSOLE_COMMAND_DEFINITION(show, next,
		"play next show program"
	) {
		IMPLEMENTS_INTERFACE_METHOD(CConsoleCommandBase::Exec());
		IShow* pShow = g_ConsoleContext.GetShow();
		if (!pShow) {
			static STR_CHUNK(cc, "show controller interface unavailable\r\n");
			Stdout(cc);
			return;
		}
		pShow->PlayNext();
	}
CONSOLE_COMMAND_DEFINITION(show, prev,
		"play previous show program"
	) {
		IMPLEMENTS_INTERFACE_METHOD(CConsoleCommandBase::Exec());
		IShow* pShow = g_ConsoleContext.GetShow();
		if (!pShow) {
			static STR_CHUNK(cc, "show controller interface unavailable\r\n");
			Stdout(cc);
			return;
		}
		pShow->PlayNext();
	}

const CConsoleCommandMapItem* AcquireConsoleCommandMapRoot() {return &g_ConsoleCommandMapItem_shell_ROOT;}

class CStreamShell :
		public CStreamFilter
{
public: // IStreamReceiver
	virtual unsigned Push(const CConstChunk& ccData) {
		IMPLEMENTS_INTERFACE_METHOD(IStreamReceiver::Push(ccData));
		g_ConsoleContext.SetCommandLine(ccData);
		if (g_ConsoleContext.GetParamsCount()) {
			const CConstChunk& ccCommandName = g_ConsoleContext.GetParam(0);
			g_ConsoleContext.Execute(ccCommandName);
		}
		return ccData.uiLength;
	}
};

CStreamShell g_StreamShell;
CStreamFilter* AcquireConsoleShell() {return &g_StreamShell;}
