#ifndef APP_PARAMS_H_INCLUDED
#define APP_PARAMS_H_INCLUDED

#include "util/callback.h"
#include <stdint.h>

#define APP_PARAMS_LIST \
	PARAM(Rover_STOP_PERIOD_MS,               uint16_t,  1000) \
	PARAM(Rover_BOOST_PERIOD_MS,              uint16_t,  0) \
	PARAM(Rover_BOOST_VALUE,                  float,     0.0) \
	PARAM(Rover_MIN_POWER,                    float,     0.0) \
	PARAM(Rover_MAX_POWER,                    float,     1.0) \
	PARAM(Rover_TOTAL_ENABLE_CHANNEL,         uint8_t,   0,      UNI_DIO_CHANNEL_DESCR) \
	PARAM(Rover_TRIAC_IRREGULARITY,           float,     0.0,    "drive output power pseudo noise value") \

#endif /* APP_PARAMS_H_INCLUDED */
