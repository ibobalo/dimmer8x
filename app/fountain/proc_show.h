#ifndef PROCESS_SHOW_H_INCLUDED
#define PROCESS_SHOW_H_INCLUDED

#include "color.h"
#include "proc_ramp.h"
#include "interfaces/display.h"

typedef IRamp<float> CRampPump;
typedef IRamp<CColorHSV> CRampLeds;

//extern
class ILed;
template <class ticktime_t> class IKey;

class IPlayerNotificationListener
{
public: // IPlayerNotificationListener
	virtual void OnPlayStarts() = 0;
	virtual void OnPlayStops() = 0;
};

class IPlayer
{
public: // IPlayer
	virtual void Play() = 0;
	virtual void Stop() = 0;
	virtual bool IsPlaying() const = 0;
	virtual void SetPlayerNotificationListener(IPlayerNotificationListener* pListener) = 0;
};

class IPlaylistEntry
{
public: // IPlaylistEntry
	virtual void Enable() = 0;
	virtual void Disable() = 0;
	virtual bool IsEnabled() const = 0;
};

class IPlaylistNotificationListener :
		public IPlayerNotificationListener
{
public: // IPlaylistNotificationListener
	virtual void OnPlaylistItemStarts() = 0;
	virtual void OnPlaylistItemStops() = 0;
};

class IPlaylist:
	public IPlayer
{
public: // IPlaylist
	virtual bool PlayNext() = 0;
	virtual bool PlayPrev() = 0;
	virtual bool PlayN(unsigned uiIndex) = 0;
	virtual bool PlayItem(IPlaylistEntry* pEntry) = 0;
	virtual unsigned GetItemsCount() const = 0;
	virtual unsigned GetCurrentItem() const = 0;
	virtual void SetPlaylistNotificationListener(IPlaylistNotificationListener* pListener) = 0;
};

class IShow :
	public IPlaylist
{
public: // IShow
};

IShow* AcquireShowInterface(
		CRampPump* apPumpRamps[],
		CRampLeds* apLightRamps[],
		ILed*      apLeds[],
		IKey<ticktime_t>* apKeys[],
		ITextDisplayDeviceMonochrome* pDisplay
);

#endif // PROCESS_SHOW_H_INCLUDED
