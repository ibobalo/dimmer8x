#ifndef FOUNTAIN_CONSOLE_H_INCLUDED
#define FOUNTAIN_CONSOLE_H_INCLUDED

#include "terminal.h"
#include "console_command.h"
#include "console_context.h"

//extern
class CStreamFilter;
class IShow;

CConsoleContext* AcquireConsoleContext(IShow* pShow);
CStreamFilter* AcquireConsoleShell();
const CConsoleCommandMapItem* AcquireConsoleCommandMapRoot();

#endif // FOUNTAIN_CONSOLE_H_INCLUDED
