export APPS_PATH:=$(abspath app)
export BOARDS_PATH:=$(abspath brd)

%:
	@$(MAKE) -C stm32framework --no-print-directory $@
