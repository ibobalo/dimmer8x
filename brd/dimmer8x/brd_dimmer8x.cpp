#include "brd_dimmer8x.h"

#include "stdafx.h"
#include "hw/hw_macros.h"
#include "hw/debug.h"
#include "hw/devices/devices.h"
#include "params.h"
#include "modules/triac/zero_sync_pwm.h"
#include "modules/keyscanner/proc_keyscanner.h"

class CTEST
{
public:
	void init() {m_test = 0;}
private:
	int m_test;
};

//static CTEST g_TEST;
/////*static*/ int CTEST::m_test;
//CTEST* getTEST() {return &g_TEST;}

class ITEST
{
public: // ITEST
	virtual void init() = 0;
};
class CTEST_InterfaceWrapper :
		public ITEST
{
public: // ITEST
	virtual void init() override;
};
void CTEST_InterfaceWrapper::init() {
//	g_TEST.init();
}

//CTEST_InterfaceWrapper g_TESTWrapper;

/*----------- USARTx Interrupt Priority -------------*/
#define USARTx_IT_PRIO              3,2

/*************************************************************************************/

#define TIMx_N          1
# define TIMx_IT_PRIO    1,1
# define TIMx_Ch1_PORT   E,9
# define TIMx_Ch2_PORT   E,11
# define TIMx_Ch3_PORT   E,13
# define TIMx_Ch4_PORT   E,14
#include "hw/mcu/hw_tim.cxx"

#define TIMx_N          2
# define TIMx_IT_PRIO    1,1
# define TIMx_Ch1_PORT   A,0
# define TIMx_Ch2_PORT   A,1
# define TIMx_Ch3_PORT   A,2
# define TIMx_Ch4_PORT   A,3
#include "hw/mcu/hw_tim.cxx"

#define TIMx_N          3
# define TIMx_IT_PRIO    1,1
# define TIMx_Ch1_PORT   B,4
# define TIMx_Ch2_PORT   B,5
# define TIMx_Ch3_PORT   B,1
# define TIMx_Ch4_PORT   B,0
#include "hw/mcu/hw_tim.cxx"
// priority of timer wrap must by higher then "zero cross" raise/fall EXTI

#define TIMx_N          4
# define TIMx_IT_PRIO    4,1
# define TIMx_Ch1_PORT   D,12
# define TIMx_Ch2_PORT   D,13
# define TIMx_Ch3_PORT   D,14
# define TIMx_Ch4_PORT   D,15
#include "hw/mcu/hw_tim.cxx"

#define TIMx_N          6
#include "hw/mcu/hw_tim.cxx"

#define TIMx_N          7
#include "hw/mcu/hw_tim.cxx"

#define TIMx_N          8
# define TIMx_IT_PRIO    1,1
# define TIMx_Ch1_PORT   C,6
# define TIMx_Ch2_PORT   C,7
#include "hw/mcu/hw_tim.cxx"

#define TIMx_N          9
# define TIMx_IT_PRIO    1,1
# define TIMx_Ch1_PORT   E,5
# define TIMx_Ch2_PORT   E,6
#include "hw/mcu/hw_tim.cxx"

#define TIMx_N          10
# define TIMx_Ch1_PORT   B,8
#include "hw/mcu/hw_tim.cxx"

#define TIMx_N          11
# define TIMx_IT_PRIO    1,1
# define TIMx_Ch1_PORT   B,9
#include "hw/mcu/hw_tim.cxx"

#ifdef USE_TRIAC
# ifdef TRIAC_EXT_ZERO
#  define DINx_PORT           A,3
# else
#  define DINx_PORT           B,3
# endif // TRIAC_EXT_ZERO
//# define EXTIx_TRIGGER        Falling
# define DINx_IT_PRIO       1,1
#include "hw/mcu/hw_din.cxx"
# ifdef TRIAC_EXT_ZERO
IDigitalInProvider* CHW_Brd_Dimmer8::AcquireTriacZeroSource() {return CHW_Brd_Dimmer8::AcquireDigitalInInterface_EXT4(true, false);}
void CHW_Brd_Dimmer8::ReleaseTriacZeroSource() {return CHW_Brd_Dimmer8::ReleaseDigitalInInterface_EXT4();};
# else
IDigitalInProvider* CHW_Brd_Dimmer8::AcquireTriacZeroSource() {return CHW_Brd_Dimmer8::AcquireDigitalInInterface_ZERO(true, false);}
void CHW_Brd_Dimmer8::ReleaseTriacZeroSource() {return CHW_Brd_Dimmer8::ReleaseDigitalInInterface_ZERO();};
# endif
#endif // USE_TRIAC

#ifdef USE_USART

// USART2 - RS485 - 1
#define UxART       USART
#define UxARTx_N        2
#define UxARTx_PORT_TX  D,5
#define UxARTx_PORT_RX  D,6
#define UxARTx_PORT_RTS D,7
#define UxARTx_IT_PRIO  USARTx_IT_PRIO
// DMA1.6 used by I2C1
//#define UxARTx_DMA_TX  1,6,4
//#define UxARTx_DMA_RX  1,5,4
#include "hw/mcu/hw_uart.cxx"  //g_HW_USART2

// USART3 - RS485 - 2
#define UxART       USART
#define UxARTx_N        3
#define UxARTx_PORT_TX  B,10
#define UxARTx_PORT_RX  B,11
#define UxARTx_PORT_RTS B,2
#define UxARTx_IT_PRIO  USARTx_IT_PRIO
// DMA1.3 used by SPI
//#define UxARTx_DMA_TX  1,3,4
// DMA1.4 used by SPI
#define UxARTx_DMA_TX    1,4,7
//#define UxARTx_DMA_RX  1,1,4
#include "hw/mcu/hw_uart.cxx" //g_HW_USART3

#endif // USE_USART

#ifdef USE_SPI
# define SPI_SPEED 10000000
// pin and resources mapping:
# define SPIx_N                     2
# define SPIx_SCK_GPIO_PORT         B,13
# define SPIx_MISO_GPIO_PORT        B,14
# define SPIx_MOSI_GPIO_PORT        B,15
// DMA1.4 used by USART3
//# define SPIx_DMA_TX              1,4,0
// DMA1.3 used by USART3
# define SPIx_DMA_RX                1,3,0
# include "hw/mcu/hw_spi.cxx"
#endif // USE_SPI

#ifdef USE_I2C
// pin and resources mapping:
# define I2Cx_N                     1
# define I2Cx_SCL_GPIO_PORT         B,6
# define I2Cx_SDA_GPIO_PORT         B,7
# define I2Cx_IT_GROUP_PRIO         3
# define I2Cx_DMA_TX                1,6,1
# define I2Cx_DMA_RX                1,0,1
# include "hw/mcu/hw_i2c.cxx"
ACQUIRE_RELEASE_I2C_DEFINITION(1, CHW_Brd_Dimmer8, I2C)
#endif // USE_I2C


#ifdef USE_ADC
#define AINx_PORT A,6,6
#  include "hw/mcu/hw_adc_in.cxx"
#define AINx_PORT A,7,7
#  include "hw/mcu/hw_adc_in.cxx"
#define AINx_PORT C,0,10
#  include "hw/mcu/hw_adc_in.cxx"
#define AINx_PORT C,1,11
#  include "hw/mcu/hw_adc_in.cxx"
#define AINx_PORT C,2,12
#  include "hw/mcu/hw_adc_in.cxx"
#define AINx_PORT C,3,13
#  include "hw/mcu/hw_adc_in.cxx"
#define AINx_PORT C,4,14
#  include "hw/mcu/hw_adc_in.cxx"
#define AINx_PORT C,5,15
#  include "hw/mcu/hw_adc_in.cxx"

#define ADCx_N 1
#include "hw/mcu/hw_adc.cxx"
#endif // USE_ADC

#ifdef USE_I2C
II2C* CHW_Brd_Dimmer8::m_pI2CMaster = NULL;
#endif // ISE_I2C

void CHW_Brd_Dimmer8::Init() {
	CHW_MCU::Init();

#ifdef USE_I2C
	m_pI2CMaster = AcquireI2CInterface_I2C(100000);
#endif // ISE_I2C

#ifdef USE_PCA9532
	IDevicePCA9532 *pPCA9532LedsBtns = GetPCA9532LedsBtnsInterface();
	pPCA9532LedsBtns->Init(m_pI2CMaster, 0, 20, 10000);
	if (LEDS_COUNT > 8) {
		IDevicePCA9532 *pPCA9532AUX = GetPCA9532AUXInterface();
		pPCA9532AUX->Init(m_pI2CMaster, 1, 0, 0);
	}
#endif // USE_PCA9532

#ifdef USE_RFM69
	if (GET_PARAM_VALUE(HW_RFM69_ENABLE)) {
		g_DeviceRFM69.Init(m_pI2CMaster);
	}
#endif // USE_RFM69

#ifdef USE_MPU6050
	if (GET_PARAM_VALUE(HW_MPU6050_ENABLE)) {
		GetMPU6050Interface()->Init(
				IDeviceMPU6050::READ_GYRO_ACCE, IDeviceMPU6050::ANGULAR_MAXRATE_250, 0, IDeviceMPU6050::LINEAR_MAXRATE_2G,
				MILLI_TO_TICKS(GET_PARAM_VALUE(HW_MPU6050_REFRESH_TIME_MS)),
				false
		);
	}
#endif // USE_MPU6050

#ifdef USE_HMC5883
	if (GET_PARAM_VALUE(HW_HMC5883_ENABLE)) {
		GetHMC5883Interface()->Init(
				IDeviceHMC5883::RATE_75_0_Hz, IDeviceHMC5883::AVERAGE_8_SAMPLES, IDeviceHMC5883::GAIN_1090_per_GS,
				MILLI_TO_TICKS(GET_PARAM_VALUE(HW_HMC5883_REFRESH_TIME_MS))
		);
	}
#endif

#ifdef USE_NET_ENC28J60
	Init_ENC28J60();
#endif // USE_NET_ENC28J60

#ifdef USE_TRIAC
	GetTriacZeroSyncPWMInterface()->Init(
			CHW_Brd::AcquireTriacZeroSource(),
#if defined(LED_ZERO) && defined(USE_KEYSCANNER)
			GetLedsInterface()->AcquireLedInterface(LED_ZERO)
#else
			NULL
#endif
	);
#endif
}

/////////////////////////////////////////////////////////////////////////////////
//
//    IOs
//
/////////////////////////////////////////////////////////////////////////////////

unsigned g_auiKbdLedRemap[8] = {0, 4, 1, 5, 2, 6, 3, 7};

void CHW_Brd_Dimmer8::SetKbdLed(unsigned nIndex)
{
#ifdef USE_PCA9532
	assert_param(nIndex < 24);
	if (nIndex < 8) {
		unsigned nIndexRemaped = g_auiKbdLedRemap[nIndex];
		GetPCA9532LedsBtnsInterface()->SetLed(nIndexRemaped);
	} else if (LEDS_COUNT > 8) {
		GetPCA9532AUXInterface()->SetLed(nIndex - 8);
	}
#endif // USE_PCA9532
}

void CHW_Brd_Dimmer8::ClearKbdLed(unsigned nIndex)
{
#ifdef USE_PCA9532
	assert_param(nIndex < 24);
	if (nIndex < 8) {
		unsigned nIndexRemaped = g_auiKbdLedRemap[nIndex];
		GetPCA9532LedsBtnsInterface()->ClearLed(nIndexRemaped);
	} else if (LEDS_COUNT > 8) {
		GetPCA9532AUXInterface()->ClearLed(nIndex - 8);
	}
#endif // USE_PCA9532
}

void CHW_Brd_Dimmer8::BlinkKbdLed(unsigned nIndex)
{
#ifdef USE_PCA9532
	assert_param(nIndex < 24);
	if (nIndex < 8) {
		unsigned nIndexRemaped = g_auiKbdLedRemap[nIndex];
		GetPCA9532LedsBtnsInterface()->BlinkLed(nIndexRemaped);
	} else if (LEDS_COUNT > 8) {
		GetPCA9532AUXInterface()->BlinkLed(nIndex - 8);
	}
#endif // USE_PCA9532
}

void CHW_Brd_Dimmer8::FastBlinkKbdLed(unsigned nIndex)
{
#ifdef USE_PCA9532
	assert_param(nIndex < 24);
	if (nIndex < 8) {
		unsigned nIndexRemaped = g_auiKbdLedRemap[nIndex];
		GetPCA9532LedsBtnsInterface()->FastBlinkLed(nIndexRemaped);
	} else if (LEDS_COUNT > 8) {
		GetPCA9532AUXInterface()->FastBlinkLed(nIndex - 8);
	}
#endif // USE_PCA9532
}

#ifdef USE_HW_IF
const IDigitalIn* CHW_Brd_Dimmer8::AcquireKbdInputInterface(unsigned nIndex)
{
	unsigned nIndexRemaped = (nIndex < ARRAY_SIZE(g_auiKbdLedRemap))
			?g_auiKbdLedRemap[nIndex]
			:nIndex;
	switch (nIndexRemaped) {
#ifdef USE_PCA9532
	// PCA9532 9-16
	case 0:  return GetPCA9532LedsBtnsInterface()->AcquireInputStateInterface(8);
	case 1:  return GetPCA9532LedsBtnsInterface()->AcquireInputStateInterface(9);
	case 2:  return GetPCA9532LedsBtnsInterface()->AcquireInputStateInterface(10);
	case 3:  return GetPCA9532LedsBtnsInterface()->AcquireInputStateInterface(11);
	case 4:  return GetPCA9532LedsBtnsInterface()->AcquireInputStateInterface(12);
	case 5:  return GetPCA9532LedsBtnsInterface()->AcquireInputStateInterface(13);
	case 6:  return GetPCA9532LedsBtnsInterface()->AcquireInputStateInterface(14);
	case 7:  return GetPCA9532LedsBtnsInterface()->AcquireInputStateInterface(15);
#endif // USE_PCA9532
	// DIO 1-8
	case 8:  return AcquireDigitalInInterface_DIO1(true);
	case 9:  return AcquireDigitalInInterface_DIO2(true);
	case 10: return AcquireDigitalInInterface_DIO3(true);
	case 11: return AcquireDigitalInInterface_DIO4(true);
	case 12: return AcquireDigitalInInterface_DIO5(true);
	case 13: return AcquireDigitalInInterface_DIO6(true);
	case 14: return AcquireDigitalInInterface_DIO7(true);
	case 15: return AcquireDigitalInInterface_DIO8(true);
	}
	return NULL;
}
#endif // USE_HW_IF

/*static*/
IDigitalOut* CHW_Brd_Dimmer8::AcquireLedOutputInterface(unsigned nIndex)
{
#ifdef USE_PCA9532
	if (nIndex < 8) {
		unsigned nIndexRemaped = g_auiKbdLedRemap[nIndex];
		return GetPCA9532LedsBtnsInterface()->AcquireLedOutputInterface(nIndexRemaped);
	} else if (LEDS_COUNT > 8) {
		return GetPCA9532AUXInterface()->AcquireLedOutputInterface(nIndex - 8);
	}
#endif
	return NULL;
}

/*static*/
void CHW_Brd_Dimmer8::ReleaseLedOutputInterface(unsigned nIndex)
{
#ifdef USE_PCA9532
	if (nIndex < 8) {
		unsigned nIndexRemaped = g_auiKbdLedRemap[nIndex];
		GetPCA9532LedsBtnsInterface()->ReleaseLedOutputInterface(nIndexRemaped);
	} else if (LEDS_COUNT > 8) {
		GetPCA9532AUXInterface()->ReleaseLedOutputInterface(nIndex - 8);
	}
#endif
}

//void CHW_Brd_Dimmer8::SetKbdChangeCallback(Callback<void (uint16_t)> cb)
//{
//#ifdef USE_PCA9532
//	GetPCA9532LedsBtnsInterface()->SetKeysChangedCallback(cb);
//#endif // USE_PCA9532
//}

#ifdef USE_USART
/*static*/
ISerial* CHW_Brd_Dimmer8::AcquireSerialInterface(unsigned uiIndex)
{
	ISerial* pSerial;
	switch (uiIndex) {
	case 0:
		pSerial = &g_HW_USART2;
		g_HW_USART2.Init();
		break;
	case 1:
		pSerial = &g_HW_USART3;
		g_HW_USART3.Init();
		break;
	default:
		pSerial = NULL;
	};
	return pSerial;
}
#endif // USE_USART

#ifdef USE_SSD1306
ITextDisplayDeviceMonochrome* CHW_Brd_Dimmer8::AcquireDisplayInterface()
{
	return AcquireSSD1306TextDisplayInterface(m_pI2CMaster);
}
#endif


#ifdef USE_RFM69
void CHW_Brd_Dimmer8::Init_RF()
{
	SPIReleaseRFM();
	INIT_GPIO_OUT(B, 12, 1); // SS
}

void CHW_Brd_Dimmer8::SPISelectRFM()
{
	GPIO_ResetBits(GPIOB, GPIO_Pin_12);
}

void CHW_Brd_Dimmer8::SPIReleaseRFM()
{
	GPIO_SetBits(GPIOB, GPIO_Pin_12);
}
#endif // USE_RFM69

#ifdef USE_NET_ENC28J60
/////////////////////////////////////////////////////////////////////////////////
//
//    ENC29J60 LAN adapter
//
/////////////////////////////////////////////////////////////////////////////////

void CHW_Brd_Dimmer8::Init_ENC28J60()
{
	SPIReleaseENC28J60();
	INIT_GPIO_OUT_FAST(A, 15, 1);   // ~SS
	INIT_GPIO_OUT(C, 14, 1);   // ~RESET
	// LAN Interrupt - config INPUT pin with Interrupt on both edges, callback to    EXTI15_10_IRQHandler     -> this->OnEXTI15_10()
	INIT_GPIO_IN_PU_FAST(C, 15); // INT
	INIT_EXTI(C, 15, 15_10, Falling, 2, 0);
}

void CHW_Brd_Dimmer8::SPISelectENC28J60()
{
	GPIO_ResetBits(GPIOA, GPIO_Pin_15);
}

void CHW_Brd_Dimmer8::SPIReleaseENC28J60()
{
	GPIO_SetBits(GPIOA, GPIO_Pin_15);
}

void CHW_Brd_Dimmer8::SPIResetENC28J60()
{
	GPIO_ResetBits(GPIOC, GPIO_Pin_14);
}

void CHW_Brd_Dimmer8::SPIClearResetENC28J60()
{
	GPIO_SetBits(GPIOC, GPIO_Pin_14);
}

bool CHW_MCU::SPIGetENC28J60Interrupt()
{
	return !GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_15);
}

void CHW_Brd_Dimmer8::OnENC28J60Interrupt()
{
	GetNetworkInterfaceENC28J60()->ScheduleRefresh();
}
#endif // USE_NET_ENC28J60
