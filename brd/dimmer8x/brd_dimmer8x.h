#ifndef HW_BRD_DIMMER8_H_INCLUDED
#define HW_BRD_DIMMER8_H_INCLUDED

#ifdef USE_BOARD_dimmer8x


#ifdef USE_TRIAC
# define USE_TRIAC_0_3
# define USE_TRIAC_4_7
#endif
#ifndef LEDS_COUNT
# define LEDS_COUNT 8
#endif
#ifndef KEYS_COUNT
# define KEYS_COUNT 16
#endif
#define LEDS_HAS_HW_BLINK
//#define KBD_HAS_CALLBACK
#define UNI_DIO_CHANNEL_DESCR    "0 - None, 1-4 - PWM1-PWM4, 5-8 - EXT1-EXT4, 9-16 - UIO1-UIO8, 17-24 - TRIAC1-TRIAC8, 25-32 - DIO1-DIO8, 33 - Zero, 34-41 - PU1-PU8, 42-49 - DOUT1-DOUT8, 50-57 - ADC1-ADC8"
#define UNI_PWM_CHANNEL_DESCR    "0 - None, 1-4 - PWM1-PWM4, 5-8 - EXT1-EXT4, 9-14 - UIO1-UIO6, 17-24 - TRIAC1-TRIAC8"
#define UNI_HBRG_CHANNEL_DESCR   "0 - None, 1 - PWM1/2, 2 - PWM3/4, 3 - EXT1/2, 4 - EXT3/4, 5 - UIO1/2,..., 7 - UIO5/6, 9 - TRIAC1/2,..., 12 - TRIAC7/8"
#define UNI_ADC_CHANNEL_DESCR    "0 - None, 1-8 - UIO1-UIO8"

#include "hw/mcu/mcu.h"
#include "hw/mcu/hw_din_dout.h"
#include "hw/mcu/hw_adc_in.h"
#include "hw/mcu/hw_tim.h"
#include "hw/mcu/hw_uart.h"
#include "hw/mcu/hw_i2c.h"
#include "hw/devices/devices.h"
#include "interfaces/display.h"


template<unsigned N, class TDO, class TDI, class TPU>
class CHW_DI_UIO
{
public:
	static inline void Acquire(bool bPullUp, bool bPullDown) { Init(bPullUp, bPullDown); }
	static inline void Release() { Deinit(); }
	static inline void Init(bool bPullUp, bool bPullDown) {
		TPU::Acquire(!bPullUp, false, false, false, false);
		TDI::Acquire();
		TDO::Acquire(!bPullDown, false, false, false, false);
	}
	static inline void Deinit() {
		TPU::Release();
		TDI::Release();
		TDO::Release();
	}
	static inline bool GetValue() {
		return TDI::GetValue();
	}
};


template<unsigned N, class TDO, class TDI, class TPU>
class CHW_DO_UIO
{
public:
	static inline void Init(bool bInitState, bool bOpenDrain, bool bPullUp, bool bPullDown, bool bFast) {
		m_bOpenDrain = bOpenDrain;
		m_bOpenSource = bPullUp && !bOpenDrain;
		TPU::Acquire(bInitState && !bOpenDrain, false, false, false, bFast);
		TDI::Acquire(false, false);
		TDO::Acquire(!bInitState && !bPullUp, false, false, false, bFast);
	}
	static inline void Set() {
		m_bState = true;
		if (!m_bOpenSource) TDO::Reset();
		if (!m_bOpenDrain) TPU::Set();
	}
	static inline void Reset() {
		m_bState = false;
		if (!m_bOpenDrain) TPU::Reset();
		if (!m_bOpenSource) TDO::Set();
	}
	static inline void Toggle() {
		if (m_bState) Reset();
		else Set();
	}
	static inline void SetValue(bool bValue) {
		if (bValue) Set(); else Reset();
	}
	static inline bool GetValue() {
		return m_bState;
	}
private:
	static bool m_bState;
	static bool m_bOpenDrain;
	static bool m_bOpenSource;
};


template <unsigned N, class TAI, class TDO, class TPU>
class CHW_AI_UIO
{
	void Init(bool bPullUp, bool bPullDown, bool bFast) {
			TPU::Acquire(!bPullUp, false, bFast);
			TAI::Acquire();
			TDO::Acquire(!bPullDown, false, bFast);
		}
		void Deinit() {
			TPU::Release();
			TAI::Release();
			TDO::Release();
		}
		unsigned GetMaxValue() const {
			return TAI::GetMaxValue();
		}
		unsigned GetValue() const {
			return TAI::GetValue();
		}
};


class CHW_Brd_Dimmer8 :
	public CHW_MCU
{
public:
	static void Init();

public: // Tims

	ACQUIRE_RELEASE_TIMER_DECLARATION(1);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(1,1,DOUT3);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(1,2,DOUT4);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(1,3,DOUT5);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(1,4,DOUT6);
	ACQUIRE_RELEASE_TIMER_DECLARATION(2);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(2,1,EXT1);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(2,2,EXT2);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(2,3,EXT3);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(2,4,EXT4);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(11,1,PWM1);
	ACQUIRE_RELEASE_TIMER_DECLARATION(3);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(3,1,TRIAC1);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(3,2,TRIAC2);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(3,3,TRIAC4);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(3,4,TRIAC3);
	ACQUIRE_RELEASE_TIMER_DECLARATION(4);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(4,1,TRIAC5);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(4,2,TRIAC6);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(4,3,TRIAC7);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(4,4,TRIAC8);
	ACQUIRE_RELEASE_TIMER_DECLARATION(5);
	ACQUIRE_RELEASE_TIMER_DECLARATION(6);
	ACQUIRE_RELEASE_TIMER_DECLARATION(7);
	ACQUIRE_RELEASE_TIMER_DECLARATION(8);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(8,1,PWM3);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(8,2,PWM4);
	ACQUIRE_RELEASE_TIMER_DECLARATION(9);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(9,1,DOUT1);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(9,2,DOUT2);
	ACQUIRE_RELEASE_TIMER_DECLARATION(10);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(10,1,PWM2);
	ACQUIRE_RELEASE_TIMER_DECLARATION(11);

#ifdef USE_HW_IF
#ifdef USE_TRIAC
public: // TRIACS
//	static void    Init_TriacZeroDetection();
	static IDigitalInProvider* AcquireTriacZeroSource(); static void ReleaseTriacZeroSource();
	static inline ITimer* AcquireTriacTimerInterface(unsigned n) {switch (n) {
		case 0: return AcquireTimerInterface_3();
		case 1: return AcquireTimerInterface_4();
		default: return NULL;}}
	static inline void ReleaseTriacTimerInterface(unsigned n) {switch (n) {
		case 0: return ReleaseTimerInterface_3();
		case 1: return ReleaseTimerInterface_4();
		default: return;}}
	static inline ITimerChannel* AcquireTriacTimerChannelInterface(unsigned n) {switch(n){
		case 0: return AcquireTimerChannelInterface_TRIAC1(0, false, true);
		case 1: return AcquireTimerChannelInterface_TRIAC2(0, false, true);
		case 2: return AcquireTimerChannelInterface_TRIAC3(0, false, true);
		case 3: return AcquireTimerChannelInterface_TRIAC4(0, false, true);
		case 4: return AcquireTimerChannelInterface_TRIAC5(0, false, true);
		case 5: return AcquireTimerChannelInterface_TRIAC6(0, false, true);
		case 6: return AcquireTimerChannelInterface_TRIAC7(0, false, true);
		case 7: return AcquireTimerChannelInterface_TRIAC8(0, false, true);
		default: return NULL;}}
	static inline void ReleaseTriacTimerChannelInterface(unsigned n) {switch(n){
		case 0: return ReleaseTimerChannelInterface_TRIAC1();
		case 1: return ReleaseTimerChannelInterface_TRIAC2();
		case 2: return ReleaseTimerChannelInterface_TRIAC3();
		case 3: return ReleaseTimerChannelInterface_TRIAC4();
		case 4: return ReleaseTimerChannelInterface_TRIAC5();
		case 5: return ReleaseTimerChannelInterface_TRIAC6();
		case 6: return ReleaseTimerChannelInterface_TRIAC7();
		case 7: return ReleaseTimerChannelInterface_TRIAC8();
		default: return;}}
#endif // USE_TRIAC
#endif // USE_HW_IF


public: // DI, DO
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,0, DIO1);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,1, DIO2);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,2, DIO3);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,3, DIO4);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(D,8, DIO5);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(D,9, DIO6);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(D,10,DIO7);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(D,11,DIO8);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,5, DOUT1);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,6, DOUT2);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,9, DOUT3);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,11,DOUT4);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,13,DOUT5);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,14,DOUT6);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(D,1, DOUT7);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(D,4, DOUT8);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,4, PU1);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,7, PU2);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,8, PU3);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,10,PU4);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,12,PU5);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,15,PU6);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(D,0, PU7);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(D,3, PU8);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(A,0, EXT1);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(A,1, EXT2);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(A,2, EXT3);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_EX_DECLARATION(A,3, EXT4);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_EX_DECLARATION(B,3, ZERO);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(B,9, PWM1);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(B,8, PWM2);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(C,7, PWM3);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(C,6, PWM4);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(A,6, ADC1);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(A,7, ADC2);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(C,0, ADC3);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(C,1, ADC4);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(C,2, ADC5);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(C,3, ADC6);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(C,4, ADC7);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(C,5, ADC8);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(A,4, DAC1);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(A,5, DAC2);

	using CHW_DI_UIO1 = CHW_DI_UIO<1, CDigitalOut_DOUT1, CDigitalIn_ADC1, CDigitalOut_PU1>;
	using CHW_DI_UIO2 = CHW_DI_UIO<2, CDigitalOut_DOUT2, CDigitalIn_ADC2, CDigitalOut_PU2>;
	using CHW_DI_UIO3 = CHW_DI_UIO<3, CDigitalOut_DOUT3, CDigitalIn_ADC3, CDigitalOut_PU3>;
	using CHW_DI_UIO4 = CHW_DI_UIO<4, CDigitalOut_DOUT4, CDigitalIn_ADC4, CDigitalOut_PU4>;
	using CHW_DI_UIO5 = CHW_DI_UIO<5, CDigitalOut_DOUT5, CDigitalIn_ADC5, CDigitalOut_PU5>;
	using CHW_DI_UIO6 = CHW_DI_UIO<6, CDigitalOut_DOUT6, CDigitalIn_ADC6, CDigitalOut_PU6>;
	using CHW_DI_UIO7 = CHW_DI_UIO<7, CDigitalOut_DOUT7, CDigitalIn_ADC7, CDigitalOut_PU7>;
	using CHW_DI_UIO8 = CHW_DI_UIO<8, CDigitalOut_DOUT8, CDigitalIn_ADC8, CDigitalOut_PU8>;

	using CHW_DO_UIO1 = CHW_DO_UIO<1, CDigitalOut_DOUT1, CDigitalIn_ADC1, CDigitalOut_PU1>;
	using CHW_DO_UIO2 = CHW_DO_UIO<2, CDigitalOut_DOUT2, CDigitalIn_ADC2, CDigitalOut_PU2>;
	using CHW_DO_UIO3 = CHW_DO_UIO<3, CDigitalOut_DOUT3, CDigitalIn_ADC3, CDigitalOut_PU3>;
	using CHW_DO_UIO4 = CHW_DO_UIO<4, CDigitalOut_DOUT4, CDigitalIn_ADC4, CDigitalOut_PU4>;
	using CHW_DO_UIO5 = CHW_DO_UIO<5, CDigitalOut_DOUT5, CDigitalIn_ADC5, CDigitalOut_PU5>;
	using CHW_DO_UIO6 = CHW_DO_UIO<6, CDigitalOut_DOUT6, CDigitalIn_ADC6, CDigitalOut_PU6>;
	using CHW_DO_UIO7 = CHW_DO_UIO<7, CDigitalOut_DOUT7, CDigitalIn_ADC7, CDigitalOut_PU7>;
	using CHW_DO_UIO8 = CHW_DO_UIO<8, CDigitalOut_DOUT8, CDigitalIn_ADC8, CDigitalOut_PU8>;

#ifdef USE_ADC
	ACQUIRE_RELEASE_ANALOG_IN_DECLARATION(6,ADC1)
	ACQUIRE_RELEASE_ANALOG_IN_DECLARATION(7,ADC2)
	ACQUIRE_RELEASE_ANALOG_IN_DECLARATION(10,ADC3)
	ACQUIRE_RELEASE_ANALOG_IN_DECLARATION(11,ADC4)
	ACQUIRE_RELEASE_ANALOG_IN_DECLARATION(12,ADC5)
	ACQUIRE_RELEASE_ANALOG_IN_DECLARATION(13,ADC6)
	ACQUIRE_RELEASE_ANALOG_IN_DECLARATION(14,ADC7)
	ACQUIRE_RELEASE_ANALOG_IN_DECLARATION(15,ADC8)

	using CHW_AI_UIO1 = CHW_AI_UIO<1, CDigitalOut_DOUT1, CAnalogIn_ADC1, CDigitalOut_PU1>;
	using CHW_AI_UIO2 = CHW_AI_UIO<2, CDigitalOut_DOUT2, CAnalogIn_ADC2, CDigitalOut_PU2>;
	using CHW_AI_UIO3 = CHW_AI_UIO<3, CDigitalOut_DOUT3, CAnalogIn_ADC3, CDigitalOut_PU3>;
	using CHW_AI_UIO4 = CHW_AI_UIO<4, CDigitalOut_DOUT4, CAnalogIn_ADC4, CDigitalOut_PU4>;
	using CHW_AI_UIO5 = CHW_AI_UIO<5, CDigitalOut_DOUT5, CAnalogIn_ADC5, CDigitalOut_PU5>;
	using CHW_AI_UIO6 = CHW_AI_UIO<6, CDigitalOut_DOUT6, CAnalogIn_ADC6, CDigitalOut_PU6>;
	using CHW_AI_UIO7 = CHW_AI_UIO<7, CDigitalOut_DOUT7, CAnalogIn_ADC7, CDigitalOut_PU7>;
	using CHW_AI_UIO8 = CHW_AI_UIO<8, CDigitalOut_DOUT8, CAnalogIn_ADC8, CDigitalOut_PU8>;
#endif // USE_ADC


#ifdef USE_I2C
	ACQUIRE_RELEASE_I2C_DECLARATION(1, I2C)
#endif

public: // KbdLeds
	static void SetKbdLed(unsigned nIndex);
	static void ClearKbdLed(unsigned nIndex);
	static void BlinkKbdLed(unsigned nIndex);
	static void FastBlinkKbdLed(unsigned nIndex);
	static const IDigitalIn* AcquireKbdInputInterface(unsigned nIndex);
	static IDigitalOut* AcquireLedOutputInterface(unsigned nIndex);
	static void ReleaseLedOutputInterface(unsigned nIndex);
//	static void SetKbdChangeCallback(Callback<void (uint16_t)> cb);

	static ISerial* AcquireSerialInterface(unsigned uiIndex);

public: // Display
	static ITextDisplayDeviceMonochrome* AcquireDisplayInterface();

public: // RFM
	friend class CSPIDeviceRFM69;
	static void SPISelectRFM();
	static void SPIReleaseRFM();

public: // ENC28J60
	static void Init_ENC28J60();
protected:
	friend class CSPIDeviceENC28J60;
	static void SPISelectENC28J60();
	static void SPIReleaseENC28J60();
	static void SPIResetENC28J60();
	static void SPIClearResetENC28J60();
	static bool SPIGetENC28J60Interrupt();
	static void OnENC28J60Interrupt();
#ifdef USE_I2C
private:
	static II2C* m_pI2CMaster;
#endif
};

typedef CHW_Brd_Dimmer8 CHW_Brd;
#endif // USE_BOARD_DIMMER8

#endif /* HW_BRD_DIMMER8_H_INCLUDED */
